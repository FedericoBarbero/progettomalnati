﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace server
{
    class MetaData
    {
        public static void sendMetaSerialized(Socket s, string file)
        {

            FileInfo fi = new FileInfo(file);
            Console.WriteLine("Meta Received e check : " + fi.FullName + " " + fi.LastWriteTime);

            BinaryFormatter bf = new BinaryFormatter();
            try
            {
                bf.AssemblyFormat = System.Runtime.Serialization.Formatters.FormatterAssemblyStyle.Simple;
                NetworkStream stm = new NetworkStream(s);
                bf.Serialize(stm, fi);
            }
            catch (Exception e)
            {
                Console.WriteLine("Error in net ser of file {0} : {1}", fi.Name, e.Message);
            }
            Console.WriteLine("Serialization over the net of file {0} completed.", fi.Name);
        }

        public static void sendMetaSerialized(Socket s, FileInfo fi)
        {
            Console.WriteLine("Meta Received e check : " + fi.FullName + " " + fi.LastWriteTime);

            BinaryFormatter bf = new BinaryFormatter();
            try
            {
                bf.AssemblyFormat = System.Runtime.Serialization.Formatters.FormatterAssemblyStyle.Simple;
                NetworkStream stm = new NetworkStream(s);
                bf.Serialize(stm, fi);
            }
            catch (Exception e)
            {
                Console.WriteLine("Error in net ser of file {0} : {1}", fi.Name, e.Message);
            }
            Console.WriteLine("Serialization over the net of file {0} completed.", fi.Name);
        }

        public static FileInfo recMetaSerialized(Socket s)
        {
            FileInfo o = null;
            try
            {
                BinaryFormatter bf = new BinaryFormatter();
                NetworkStream stm = new NetworkStream(s);
                o = (FileInfo)bf.Deserialize(stm);
                Console.WriteLine("Meta Received : " + o.FullName + " " + o.LastWriteTime);
                return o;
            }
            catch (Exception e)
            {
                Console.WriteLine("Error in deserialization from the network. " + e.Message + e.StackTrace);
            }
            return null;
        }

        public static void saveMetaSerialized(string file, FileInfo obj)
        {
            Console.WriteLine("Meta Received e check : " + obj.FullName + " " + obj.LastWriteTime);


            if (obj == null)
                throw new Exception("Error! File info obj is null!");
            BinaryFormatter bf = new BinaryFormatter();
            try
            {
                using (FileStream ms = new FileStream(file, FileMode.Create))
                {
                    bf.Serialize(ms, obj);

                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error in file ser of file {0} : {1}", obj.Name, e.Message);
            }
            Console.WriteLine("Serialization over the net of file {0} completed.", obj.Name);
        }
        public static FileInfo readMetaSerialized(string file)
        {
            FileInfo o = null;
            try
            {
                BinaryFormatter bf = new BinaryFormatter();
                FileStream stm = new FileStream(file, FileMode.Open);
                o = (FileInfo)bf.Deserialize(stm);
                Console.WriteLine("Meta Received e check : " + o.FullName + " " + o.LastWriteTime);
                return o;
            }
            catch (Exception e)
            {
                Console.WriteLine("Error in deserialization from the file. " + e.Message + e.StackTrace);
            }
            return null;
        }



    }
}
