﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using server;
using System.Net.Sockets;
using System.IO;
using System.Collections;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;

/*  Backup Restore Add Update Delete */

namespace server
{
    class Braud
    {
        public static ArrayList nomi = new ArrayList();
 

        public static void backupHandle(Socket clientSock, string user)
        {

            try
            {
                //FunctionArgs fa = (FunctionArgs)o;
                // clientSock = (Socket) o ;
                //String user = fa.user;
                String s2 = Program.riceviFile(clientSock);

                using (StreamReader sr2 = new StreamReader(s2))
                {
                    // Console.WriteLine("{0}", sr2.ReadToEnd());

                    Directory.CreateDirectory(Program.root + user + "\\" + "fslog");
                    Directory.CreateDirectory(Program.root + user + "\\" + "filesystem");
                    Directory.CreateDirectory(Program.root + user + "\\" + "metadati");

                    File.Copy(s2, Program.root + user + "\\" + "fslog" + "\\" + Program.NomeFileDaPath(s2));

                    string line;
                    string filename = null;
                    int n = 0;

                    while ((line = sr2.ReadLine()) != null)
                    {
                        line = Program.SeparaPathDaMd5(line);
                        string nome = Program.NomeFileDaPath(line);
                        if (nome != null)
                        {
                            try
                            {
                                n++;
                                filename = Program.FileDownload(clientSock, Program.root + user + "\\" + "filesystem" + "\\", n.ToString(), user);
                                /* se il file è duplicato appendo un numero */

                                Program.VersionModify(Program.root + user + "\\" + "filesystem", filename);
                                Program.InviaAck(clientSock);
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(" Errore nella ricezione del file {0} : \n {1}", nome, ex.StackTrace);
                                RollBackF(filename, user);
                                File.Delete(Program.UltimoLogFile(user));
                                return;
                            }

                            try
                            {
                                /* deserializzo metadati dalla rete*/
                                //FileInfo fi;
                                ///* serializzo metadati su file */
                                //fi = MetaData.recMetaSerialized(clientSock);
                                //MetaData.saveMetaSerialized(Program.root + user + "\\" + "metadati" + "\\" + filename, fi);
                                //Program.VersionModify(Program.root + user + "\\" + "metadati", filename);
                                //Program.InviaAck(clientSock);
                                filename = Program.FileDownload(clientSock, Program.root + user + "\\" + "metadati" + "\\", n.ToString(), user);
                                Program.VersionModify(Program.root + user + "\\" + "metadati", filename);
                                Program.InviaAck(clientSock);

                            }
                            catch (Exception ex2)
                            {
                                Console.WriteLine("Errore nella ricezioni dei metadati di {0} : \n {1}", nome, ex2.StackTrace);
                                RollBackM(filename, user);
                            }


                        }


                    }

                }
                File.Delete(s2);
               
            }
            catch (Exception e)
            {
                Console.WriteLine("Il client ha chiuso la connessione durante il backup \n " +
                "il sistema torna all'ultimo stato stabile ");
                throw e;
            }


        }

        public static void restoreHandle(Socket clientSock, string user)
        {
            string filename = null;
            int num = 0;
            try
            {
                byte[] ackmsg = new byte[100];
                int check;

                check = clientSock.Receive(ackmsg);
                if (check == 0) throw new Exception();
                string filedarecuperare = Encoding.ASCII.GetString(ackmsg);

                /* /root/ect...*/

                filedarecuperare = filedarecuperare.TrimEnd(new char[] { '\0' });

                num = Program.NumeroFileDaLog(user, filedarecuperare);
                check = 0 ; // conto quante versioni del file sono presenti 
                if (File.Exists(Program.root + user + "\\filesystem\\" + num.ToString() + "_v1"))
                    check++;
                if (File.Exists(Program.root + user + "\\filesystem\\" + num.ToString() + "_v2"))
                    check++;
                if (File.Exists(Program.root + user + "\\filesystem\\" + num.ToString() + "_v3"))
                    check++;

                /* devo inviare il tempo di ultima modifica */

                byte[] versioni = new byte[4];
                versioni = BitConverter.GetBytes(check);
                clientSock.Send(versioni);

                if (check == 0) return; 

                while (check != 0)
                {
                    //FileInfo fi = new FileInfo(Program.root + user + "\\filesystem\\" + num + "_v" + check);
                    // http://stackoverflow.com/questions/1446547/how-to-convert-an-object-to-a-byte-array-in-c-sharp
                    // fede deve copiare la deserialize 
                    // invio un 
                    //versioni2 = ObjectToByteArray(fi);

                    /*uso la sendMetadata */
                    
                    //FileInfo fi =  MetaData.readMetaSerialized(Program.root + user + "\\metadati\\" + num + "_v" + check);
                    //Console.WriteLine("Last write time : " + fi.LastWriteTime.Date + " day : " + fi.LastWriteTime.Day + " Month : " + fi.LastWriteTime.Month +
                    //    " Year : " + fi.LastWriteTime.Year + " Hour : " + fi.LastWriteTime.Hour + " Minutes : " + fi.LastWriteTime.Minute);
                    //MetaData.sendMetaSerialized(clientSock, fi);

                    Program.FileUpload(clientSock, Program.root + user + "\\metadati\\" + num + "_v" + check, user,1.0);
                    //Program.AttendiAck(clientSock);
                    Thread.Sleep(100);

                  //versioni2 =  BitConverter.GetBytes(dt);
                    check--;
                }

                clientSock.Receive(versioni);

                check = BitConverter.ToInt32(versioni, 0);

                /* da scrivere */

                Program.FileUpload(clientSock, num.ToString() + "_v" + check,user);
                Program.AttendiAck(clientSock);

            }
            catch (Exception e)
            {
                Console.WriteLine("Errore nella ricezione del nome file da recuperare : {0} ", e.StackTrace);
            }

            /* per i duplicati in cartelle diverse siccome appendo un numero devo rileggere l'ultimo file di log
             * e trovare questo numero e per poter restituire esattamente il file richiesto */

            //foreach (string f in Directory.GetFiles(Program.root + user + "\\metadati"))
            //{
            //    string fileversion = Program.root + user + "\\metadati\\" + filename;
            //    if (fileversion.CompareTo(f)==1)
            //    {
            //        MetaData.sendMetaSerialized(clientSock, f);
            //    }

            //}


        }

        // Convert an object to a byte array
        public static byte[] ObjectToByteArray(Object obj)
        {
            BinaryFormatter bf = new BinaryFormatter();
            using (var ms = new MemoryStream())
            {
                bf.Serialize(ms, obj);
                return ms.ToArray();
            }
        }

        public static void addHandle(Socket clientSock,string user)
        {
            string filename =null;
            int num = 0;
            try
            {
                string line;
                using (StreamReader file = new StreamReader(Program.UltimoLogFile(user)))
                {

                    while ((line = file.ReadLine()) != null)
                    {
                        //line = Program.SeparaPathDaMd5(line);
                        num++;
                    }
                    if(File.Exists( Program.root + user + "\\filesystem\\"+ num.ToString()+"_v1" ))  num++;
                    filename = Program.FileDownload(clientSock, Program.root + user + "\\filesystem\\", num.ToString(), user);
                    Program.VersionModify(Program.root + user + "\\filesystem", filename);
                    Program.InviaAck(clientSock);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(" Errore nella ricezione del file {0} : \n {1} ",filename,ex.StackTrace);
                return;
            }

            try
            {
                /* deserializzo metadati dalla rete*/
                //FileInfo fi;
                ///* serializzo metadati su file */
                //fi = MetaData.recMetaSerialized(clientSock);
                //MetaData.saveMetaSerialized(Program.root + user + "\\" + "metadati" + "\\" + filename, fi);
                //Program.VersionModify(Program.root + user + "\\" + "metadati", filename);
                ////Program.InviaAck(clientSock);
                filename = Program.FileDownload(clientSock, Program.root + user + "\\metadati\\", num.ToString(), user);
                Program.VersionModify(Program.root + user + "\\metadati", filename);
                Program.InviaAck(clientSock);

            }
            catch (Exception ex2)
            {
                Console.WriteLine("Errore nella ricezioni dei metadati di un file {0} : \n {1} ", filename, ex2.StackTrace);
                RollBackM(filename, user);
            }

        }

        public static void updateHandle(Socket clientSock, string user)
        {
            string filename=null; 
            int num;
            try
            {
                filename = Program.FileDownload(clientSock, Program.root +  user + "\\filesystem\\");

                Console.WriteLine("File da modificare : " + filename);

                string file = Program.NomeFileDaPath(filename);

                num = Program.NumeroFileDaLog(user, filename);
                File.Move(Program.root + user + "\\filesystem\\" + file, Program.root + user + "\\filesystem\\" + num.ToString());
                Program.VersionModify(Program.root + user + "\\" + "filesystem", num.ToString());
                //File.Delete(Program.root + user + "\\" + "filesystem" + num.ToString() + "_v1");
                //File.Move(Program.root + user + "\\filesystem" + file, Program.root + user + "\\filename" + num.ToString() + "_v1");
                Program.InviaAck(clientSock);
            }
            catch (Exception e)
            {
                Console.WriteLine(" Errore nella ricezione del file {0} : \n {1} ", filename, e.StackTrace);
                return;
            }
            try
            {
                //FileInfo fi;
                //fi = MetaData.recMetaSerialized(clientSock);

                //Console.WriteLine("Meta Received : " + fi.FullName + " " + fi.LastWriteTime);
                //MetaData.saveMetaSerialized(Program.root + user + "\\" + "metadati" + "\\" + num.ToString(), fi);
                //Program.VersionModify(Program.root + user + "\\" + "metadati", num.ToString());
               // Program.VersionModify(Program.root + user + "\\" + "metadati", filename);
               // Program.InviaAck(clientSock);
                filename = Program.FileDownload(clientSock, Program.root + user + "\\metadati\\", num.ToString(), user);
                Program.VersionModify(Program.root + user + "\\metadati", filename);
                Program.InviaAck(clientSock);

            }
            catch (Exception ex)
            {
                Console.WriteLine("Errore nella ricezioni dei metadati di un file {0} : \n {1} ", filename, ex.StackTrace);
            }

           ScalaFilesystem(user,num);
           ScalaMetadati(user, num);

        }

        public static string DupHandle(string filename)
        {
            if (nomi.Contains(filename))
            {
                filename = filename + "_2";
            }
            else
                nomi.Add(filename);

            return filename;
        }

        public static void RollBackF(string filename, string user)
        {
            if (File.Exists(Program.root + user + "\\filesystem\\" + filename))
                File.Delete(Program.root + user + "\\filesystem\\" + filename);
        }

        public static void RollBackM(string filename, string user)
        {
            if (File.Exists(Program.root + user + "\\metadati\\" + filename))
                File.Delete(Program.root + user + "\\metadati\\" + filename);
        }



        /* gestione dei duplicati */ 

         public static void delHandle(Socket clientSock, string user)
        {
            byte[] ackmsg = new byte[250];
            clientSock.Receive(ackmsg);
            string filedaeliminare = Encoding.ASCII.GetString(ackmsg);

            //string file = filedaeliminare.Replace("\0", string.Empty); 

            filedaeliminare  =  filedaeliminare.TrimEnd(new char[] { '\0' });

            Console.WriteLine("File da eliminare : " + filedaeliminare);
            
//            string filename = Program.NomeFileDaPath(filedaeliminare);

            int num = Program.NumeroFileDaLog(user,filedaeliminare);
            //string filename = Program.NomeFileDaNumero(Program.root + user + "\\filesystem", num);
            try
            {
                if (File.Exists(Program.root + user + "\\filesystem\\" + num.ToString() + "_v1"))
                {
                    Thread.Sleep(100);
                    File.Delete(Program.root + user + "\\filesystem\\" + num.ToString() + "_v1");
                }
                if (File.Exists(Program.root + user + "\\filesystem\\" + num.ToString() + "_v2"))
                {
                    Thread.Sleep(100);
                    File.Delete(Program.root + user + "\\filesystem\\" + num.ToString() + "_v2");

                }
                if (File.Exists(Program.root + user + "\\filesystem\\" + num.ToString() + "_v3"))
                {
                    Thread.Sleep(100);
                    File.Delete(Program.root + user + "\\filesystem\\" + num.ToString() + "_v3");
                }
                    

                if (File.Exists(Program.root + user + "\\metadati\\" + num.ToString() + "_v1"))
                {
                    Thread.Sleep(100);
                    File.Delete(Program.root + user + "\\metadati\\" + num.ToString() + "_v1");
                }
                    
                if (File.Exists(Program.root + user + "\\metadati\\" + num.ToString() + "_v2"))
                {
                    Thread.Sleep(100);
                    File.Delete(Program.root + user + "\\metadati\\" + num.ToString() + "_v2");
                }
                    
                if (File.Exists(Program.root + user + "\\metadati\\" + num.ToString() + "_v3"))
                {
                    Thread.Sleep(100);
                    File.Delete(Program.root + user + "\\metadati\\" + num.ToString() + "_v3");
                }
                    
            }
             catch(Exception ex)
            {
                Console.WriteLine("Errore nella file delete: " + ex.Message + " " + ex.StackTrace);
                throw ex;
            }

            RicompattaFilesystem(user);
            RicompattaMetadati(user);
            
        }

         public static void ScalaMetadati(string user, int n)
         {
             int check = 1;

             while (File.Exists(Program.root + user + "\\metadati\\" + (n).ToString() + "_v" + check))
             {
                 File.Move(Program.root + user + "\\metadati\\" + (n).ToString() + "_v" + check.ToString(), Program.root + user + "\\metadati\\" + "tmp" + "_v" + check.ToString());
                 check++;
             }

             while (File.Exists(Program.root + user + "\\metadati\\" + (n + 1).ToString() + "_v1"))
             {
                 check = 1; // conto quante versioni del file sono presenti 

                 if (File.Exists(Program.root + user + "\\metadati\\" + (n + 1).ToString() + "_v2"))
                     check++;
                 if (File.Exists(Program.root + user + "\\metadati\\" + (n + 1).ToString() + "_v3"))
                     check++;

                 while (check != 0)
                 {
                     File.Move(Program.root + user + "\\metadati\\" + (n + 1).ToString() + "_v" + check.ToString(), Program.root + user + "\\metadati\\" + (n).ToString() + "_v" + check.ToString());
                     check--;
                 }

                 n++;
             }

             check = 1;
             while (File.Exists(Program.root + user + "\\metadati\\" + "tmp" + "_v" + check))
             {
                 File.Move(Program.root + user + "\\metadati\\" + "tmp" + "_v" + check.ToString(), Program.root + user + "\\metadati\\" + n + "_v" + check.ToString());
                 check++;
             }


         }



         public static void ScalaFilesystem(string user,int n)
         {
             int check=1;

             while (File.Exists(Program.root + user + "\\filesystem\\" + (n).ToString() + "_v" + check))
             {
                 File.Move(Program.root + user + "\\filesystem\\" + (n).ToString() + "_v" + check.ToString(), Program.root + user + "\\filesystem\\" + "tmp" + "_v" + check.ToString());
                 check++;
             }

             while (File.Exists(Program.root + user + "\\filesystem\\" + (n + 1).ToString() + "_v1"))
             {
                 check = 1; // conto quante versioni del file sono presenti 

                 if (File.Exists(Program.root + user + "\\filesystem\\" + (n + 1).ToString() + "_v2"))
                     check++;
                 if (File.Exists(Program.root + user + "\\filesystem\\" + (n + 1).ToString() + "_v3"))
                     check++;

                 while (check != 0)
                 {
                     File.Move(Program.root + user + "\\filesystem\\" + (n + 1).ToString() + "_v" + check.ToString(), Program.root + user + "\\filesystem\\" + (n).ToString() + "_v" + check.ToString());
                     check--;
                 }

                 n++;
             }

             check = 1;
             while (File.Exists(Program.root + user + "\\filesystem\\" + "tmp" + "_v" + check))
             {
                 File.Move(Program.root + user + "\\filesystem\\" + "tmp" + "_v" + check.ToString(), Program.root + user + "\\filesystem\\" + n + "_v" + check.ToString());
                 check++;
             }


         }

         public static void RicompattaFilesystem(string user)
         {
             int n=1;
             string nome="";
             //foreach (var f in Directory.GetFiles(Program.root + user + "\\filesystem"))
             //{
             //    nome = Program.NomeFileDaPath(f,"_");
             //    if (nome.Equals(n.ToString()+"_v1"))
             //    {
             //        n++;
             //    }
             //    else
             //        break;
             //}

             while (nome != null)
             {
                 if (File.Exists(Program.root + user + "\\filesystem\\" + n + "_v1"))
                     n++;
                 else nome =null;
             }

             int check;

             while (File.Exists(Program.root + user + "\\filesystem\\" + (n + 1).ToString()+"_v1"))
             {
                 check = 1; // conto quante versioni del file sono presenti 
                 
                 if (File.Exists(Program.root + user + "\\filesystem\\" + (n+1).ToString() + "_v2"))
                     check++;
                 if (File.Exists(Program.root + user + "\\filesystem\\" + (n+1).ToString() + "_v3"))
                     check++;

                 while (check != 0)
                 {
                     Thread.Sleep(1000);
                     File.Move(Program.root + user + "\\filesystem\\" + (n + 1).ToString() + "_v" + check.ToString(), Program.root + user + "\\filesystem\\" + (n).ToString() + "_v" + check.ToString());
                     check--;
                 }

                 n++;
             }


         }

         public static void RicompattaMetadati(string user)
         {
             int n = 1;
             string nome="";
             //foreach (var f in Directory.GetFiles(Program.root + user + "\\metadati"))
             //{
             //    nome = Program.NomeFileDaPath(f, "_");
             //    if (nome.Equals(n.ToString() + "_v1"))
             //    {
             //        n++;
             //    }
             //    else
             //        break;
             //}


             while (nome != null)
             {
                 if (File.Exists(Program.root + user + "\\metadati\\" + n + "_v1"))
                     n++;
                 else nome = null;
             }

             int check;

             while (File.Exists(Program.root + user + "\\metadati\\" + (n + 1).ToString() + "_v1"))
             {
                 check = 1; // conto quante versioni del file sono presenti 

                 if (File.Exists(Program.root + user + "\\metadati\\" + (n + 1).ToString() + "_v2"))
                     check++;
                 if (File.Exists(Program.root + user + "\\metadati\\" + (n + 1).ToString() + "_v3"))
                     check++;

                 while (check != 0)
                 {
                     Thread.Sleep(100);

                     try
                     {
                         File.Move(Program.root + user + "\\metadati\\" + (n + 1).ToString() + "_v" + check.ToString(), Program.root + user + "\\metadati\\" + (n).ToString() + "_v" + check.ToString());
                         check--;
                     }
                     catch (Exception ex)
                     {
                         Console.WriteLine(ex.StackTrace);
                         throw ex;      
                     }
                 }

                 n++;
             }


         }



    }
}
