﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace server
{
    class Version
    {
        /* passo una directory e ne faccio la stampa su testo 
         *  passo il percorso path
         * il nome del filesystem log che verrà creato da questa funzione
         * il nome della cartella root dell'utente string utente = @"\davide"; 
         * 
               nella cartella prova ho tutto il codice per fare le versioni del file di log
         */
         
        public static void FileSystemLog(string path, string filesystemstruct,string rootUser)
        {

            try
            {
                if (!File.Exists(filesystemstruct))
                {
                    using (StreamWriter sw = File.CreateText(filesystemstruct)) { }
                }
                foreach (string d in Directory.GetDirectories(path))
                {
                    foreach (string f in Directory.GetFiles(d))
                    {
                        using (StreamWriter sw = File.AppendText(filesystemstruct))
                        {
                            //sw = File.AppendText(filesystemstruct);
                            char[] delimiterChars = { '\\' };
                            string[] words = f.Split(delimiterChars);

                            //string stampa = @"\repo";
                            int flag = 0;
                            foreach (string s in words)
                            {
                                if (flag != 0)
                                    rootUser += "\\" + s;
                                if (s.CompareTo(rootUser) == 0)
                                    flag = 1;
                            }
                            sw.WriteLine(rootUser);
                        }
                        //}

                    }

                    FileSystemLog(d, filesystemstruct,rootUser);
                }
            }
            catch (System.Exception excpt)
            {
                Console.WriteLine("Errore nella scrittura ricorsiva : " + excpt.Message);
            }
        }

        /* path  =  percorso della cartella, nome file semplice non path del file
         * mi ritorna il nome del nuovo file che dovrò andare a riempire con i byte del client
         
         al di là del cambio di versioni il nome del nuovo e più recente file sarà sempre :
          path + "\\" + filename + "_v1";                                                 */

        public static void VersionModify(string path,string filename)
        {
            if (!File.Exists(path + "\\" + filename+"_v1"))
            {
                File.Create(path + "\\" + filename + "_v1");
                return;
            }
            if (!File.Exists(path + "\\" + filename + "_v2"))
            {
                File.Move(path + "\\" + filename + "_v1", path + "\\" + filename + "_v2");
                File.Create(path + "\\" + filename + "_v1");
                return;
            }
            if (!File.Exists(path + "\\" + filename + "_v3"))
            {
                File.Move(path + "\\" + filename + "_v2", path + "\\" + filename + "_v3");
                File.Move(path + "\\" + filename + "_v1", path + "\\" + filename + "_v2");
                File.Create(path + "\\" + filename + "_v1");
                return;
            }

            File.Delete(path + "\\" + filename + "_v3");
            File.Move(path + "\\" + filename + "_v2", path + "\\" + filename + "_v3");
            File.Move(path + "\\" + filename + "_v1", path + "\\" + filename + "_v2");
            File.Create(path + "\\" + filename + "_v1");
        }

    }
}
