﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization.Json;
using System.Data;
using System.Data.SqlClient;
using server;
using System.Collections;

namespace server
{
    class Program
    {
        public static string root = @"C:\Users\Davide\Documents\progetto malnati\server\root\";
        //public volatile bool TerminateThread = false;

        /*  funzione legata al thread incaricato di servire un singolo client */
        // eliminami 
        public void ClientHandler(Object socket)
        {
            Socket clientSock = (Socket)socket;
            run(clientSock);
        }

        /* funzione che riceve un semplice file di testo lo salva localmente e ne ritorna il path */ 

        public static String riceviFile(Socket clientSock)
        {
            try
            {
                byte[] clientData = new byte[1024 * 5000];

                //Directory.CreateDirectory(receivedPath);
                int receivedBytesLen = clientSock.Receive(clientData);

                int fileNameLen = BitConverter.ToInt32(clientData, 0);
                string fileName = Encoding.ASCII.GetString(clientData, 4, fileNameLen);

                Console.WriteLine("Client:{0} connected & File {1} started received.", clientSock.RemoteEndPoint, fileName);

                IPEndPoint remote = clientSock.RemoteEndPoint as IPEndPoint;

                String s = root + Path.GetFileNameWithoutExtension(fileName) + remote.Address + ".txt";
                using (BinaryWriter bWrite = new BinaryWriter(File.Open(s, FileMode.Create)))
                {
                    bWrite.Write(clientData, 4 + fileNameLen, receivedBytesLen - 4 - fileNameLen);

                    Console.WriteLine("File: {0} received & saved at path: {1}", fileName, root);
                }

                return s;
            }
            catch(Exception e){

                Console.WriteLine("Ricevi File fallita : Errore nella ricezione di un file di gestione del protocollo");
                throw e; 
            }
            
        }

        /* lancio il thread principale con il loop del server */

        public void run(Object o){

            Socket clientSock = (Socket)o;
            string s=null;
            try
            {
                s = riceviFile(clientSock);
            }
            catch(Exception e){
                Console.WriteLine("Errore nella run "+e.StackTrace);
                return;
            }

            /* stampe di debug del messaggio ricevuto dal client */

            using (StreamReader sr = new StreamReader(s))
            {
                Console.WriteLine("credenziali registrazione o login");

                String type = sr.ReadLine();
                Console.WriteLine("Tipo : " + type);
                String user = sr.ReadLine();
                Console.WriteLine("username : " + user);
                String pass = sr.ReadLine();
                Console.WriteLine("password :" + pass);
                sr.Close();


                /* fine stampe di debug */

                File.Delete(s);

                DBManagement db = new DBManagement();
                SqlConnection sc;
                sc = db.ConnectDb();

                if (type.Equals("reg"))
                {
                    if (reg(clientSock, db, sc, user, pass))
                        Directory.CreateDirectory(root + user);
                    return;
                }
                else if (log(clientSock, db, sc, user, pass))
                {
                    byte[] ackmsg = new byte[3];
                    int check;

                    try
                    {
                        string filelog = UltimoLogFile(user);
                        if (filelog != null)
                        {
                            ackmsg = Encoding.ASCII.GetBytes("XYZ");
                            clientSock.Send(ackmsg);
                            FileUpload(clientSock, filelog, user, 0);
                            Console.WriteLine("é già presente un backup per questo utente");
                        }
                        else
                        {
                            ackmsg = Encoding.ASCII.GetBytes("FRS");
                            clientSock.Send(ackmsg);
                            Console.WriteLine("Primo accesso per questo utente");

                        }


                        while (true)
                        {
                            check = clientSock.Receive(ackmsg);
                            //Thread.Sleep(2000);
                            if (check == 0) throw new Exception();
                            string back_or_rest = Encoding.ASCII.GetString(ackmsg);
                            switch (back_or_rest)
                            {
                                case "BAK":
                                    {
                                        Console.WriteLine("Client {0} sta effettuando un'operazione di BACKUP", clientSock.RemoteEndPoint);
                                        //ThreadPool.QueueUserWorkItem(new WaitCallback(backupHandle), fa);
                                        Braud.backupHandle(clientSock, user);
                                    }
                                    break;

                                case "RES":
                                    {
                                        Console.WriteLine("Client {0} sta effettuando un'operazione di RESTORE", clientSock.RemoteEndPoint);
                                        check = clientSock.Receive(ackmsg);
                                        if (check == 0) throw new Exception();
                                        back_or_rest = Encoding.ASCII.GetString(ackmsg);


                                        
                                        if (back_or_rest.Equals("CNT"))
                                        {
                                            Braud.restoreHandle(clientSock, user);
                                            // InviaAck(clientSock);
                                        }
                                        if (back_or_rest.Equals("ABR"))
                                        {
                                            Console.WriteLine("Restore aborted");
                                        }

                                    }
                                    break;

                                case "ADD":
                                    {
                                        Console.WriteLine("Client {0} sta effettuando un'operazione di ADD di un file", clientSock.RemoteEndPoint);
                                        Braud.addHandle(clientSock, user);
                                        //InviaAck(clientSock);
                                    }
                                    break;

                                case "UPD":
                                    {
                                        Console.WriteLine("Client {0} sta effettuando un'operazione di UPDATE di un file", clientSock.RemoteEndPoint);
                                        /* gestiamo il caso in cui il client faccia un restore e poi 
                                         * decida di tornare sui propri passi */
                                        //check = clientSock.Receive(ackmsg);
                                        //Thread.Sleep(2000);
                                        Braud.updateHandle(clientSock, user);
                                        InviaAck(clientSock);

                                    }
                                    break;

                                case "DEL":
                                    {
                                        Console.WriteLine("Client {0} sta effettuando un'operazione di DELETE di un file", clientSock.RemoteEndPoint);
                                        Braud.delHandle(clientSock, user);
                                        InviaAck(clientSock);
                                    }
                                    break;

                                case "LOG":
                                    {
                                        String s2 = Program.riceviFile(clientSock);
                                        if (File.Exists(Program.root + user + "\\" + "fslog" + "\\" + Program.NomeFileDaPath(s2)))
                                            File.Delete(Program.root + user + "\\" + "fslog" + "\\" + Program.NomeFileDaPath(s2));
                                        File.Move(s2, Program.root + user + "\\" + "fslog" + "\\" + Program.NomeFileDaPath(s2));
                                        InviaAck(clientSock);
                                    }
                                    break;

                                default:
                                    throw new Exception("Operazione scelta dal client non corretta");
                            }


                            ackmsg = null;
                            ackmsg = new byte[3];

                        }

                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Client {0} ha chiuso la connessione : \n{1}", clientSock.RemoteEndPoint, e.StackTrace);
                        return;
                    }

                }


                else
                    return;

                db.CloseDB(sc);
            }
        }

        public static int NumeroFileDaLog(string user,string filepath)
        {
            string log = UltimoLogFile(user);
            int n = 0;
            string line;

            using (StreamReader file = new StreamReader(log))
            {
                while ((line = file.ReadLine()) != null)
                {
                    line = SeparaPathDaMd5(line);
                    if (!line.Equals(filepath))
                    {
                        n++;
                    }
                    else
                        return ++n;
                }

            }

            return -1;
        }

        public static string UltimoLogFile(string user)
        {
            string path = root + user + "\\fslog";
            try
            {
                var directory = new DirectoryInfo(path);
                var myFile = (from f in directory.GetFiles()
                              orderby f.LastWriteTime descending
                              select f).First();
                if (myFile != null)
                    return myFile.FullName;
            }
            catch (Exception e)
            {
                return null;
            }
            return null;

        }

        public static void VersionModify(string path, string filename)
        {
            try
            {
                if (!File.Exists(path + "\\" + filename + "_v1"))
                {
                    File.Move(path + "\\" + filename, path + "\\" + filename + "_v1");
                    // File.Create(path + "\\" + filename + "_v1");
                    return;
                }
                if (!File.Exists(path + "\\" + filename + "_v2"))
                {
                    File.Move(path + "\\" + filename + "_v1", path + "\\" + filename + "_v2");
                    File.Move(path + "\\" + filename, path + "\\" + filename + "_v1");
                    return;
                }
                if (!File.Exists(path + "\\" + filename + "_v3"))
                {
                    File.Move(path + "\\" + filename + "_v2", path + "\\" + filename + "_v3");
                    File.Move(path + "\\" + filename + "_v1", path + "\\" + filename + "_v2");
                    File.Move(path + "\\" + filename, path + "\\" + filename + "_v1");
                    return;
                }

                File.Delete(path + "\\" + filename + "_v3");
                File.Move(path + "\\" + filename + "_v2", path + "\\" + filename + "_v3");
                File.Move(path + "\\" + filename + "_v1", path + "\\" + filename + "_v2");
                File.Move(path + "\\" + filename, path + "\\" + filename + "_v1");

            }

            catch (Exception e)
            {
                Console.WriteLine("Version Modify : errore nello scalare le versioni");
                throw e;
            }

        }

        public static string FileDownload(Socket s,string path,string newname,string user)
        {

                    try
                    {

                        byte[] clientData = new byte[150000];

                        int receivedBytesLen = s.Receive(clientData);

                        /* prima invio un intero che indica la lunghezza del nome del file */

                        int fileNameLen = BitConverter.ToInt32(clientData, 0);

                        /* poi leggo il nome del file e lo salvo in una stringa */

                        string fileName = Encoding.ASCII.GetString(clientData, 4, fileNameLen);

                        fileName = newname;

                        long lunghezzaFile = BitConverter.ToInt32(clientData, 4 + fileNameLen);

                        // stampa di debug

                        Console.WriteLine("Client:{0} connesso & File {1} inizio ricezione.", s.RemoteEndPoint, fileName);

                        using (BinaryWriter bWrite = new BinaryWriter(File.Open(path + fileName, FileMode.Append)))
                        {
                            bWrite.Write(clientData, 12 + fileNameLen, receivedBytesLen - 8 - fileNameLen - 4);

                            int letti = receivedBytesLen - 12 - fileNameLen;

                            long tot = lunghezzaFile-letti;
                            int totletti = letti;
                            lunghezzaFile -= letti;

                            //Console.Write("Trasferimento di {0} : ", fileName);
                          
                                while (lunghezzaFile > 0)
                                {
                                    letti = s.Receive(clientData);
                                   
                                    bWrite.Write(clientData, 0, letti);
                                    totletti += letti;
                                    
                                    //progress.Report((double)totletti / tot);
                                    double ttt = ((double)totletti /(double) tot);
                                    ttt*=100;
                                    Console.Write("\r Trasferimento di {0} :  {1}%   ", fileName, (int) ttt);
                                    //Thread.Sleep(5);
                                    lunghezzaFile -= letti;

                                }

                                Console.Write("\r Trasferimento di {0} :  {1}%   ==> ", fileName, 100);
                                Console.WriteLine("Fatto");

                          
                            Console.WriteLine("File: {0} ricevuto e salvato al path: {1}", fileName, path);




                            return fileName;

                        }
                    }
                    
                    catch (Exception ex)
                    {
                        Console.WriteLine("Download Fallito : " + ex.Message);
                        //bWrite.Close();
                        Braud.RollBackF(newname, user);

                        throw ex;
                    }
          
        }

        public static string FileDownload(Socket s, string path)
        {
            try
            {

                byte[] clientData = new byte[150000];

                int receivedBytesLen = s.Receive(clientData);

                /* prima invio un intero che indica la lunghezza del nome del file */

                int fileNameLen = BitConverter.ToInt32(clientData, 0);

                /* poi leggo il nome del file e lo salvo in una stringa */

                string fileName = Encoding.ASCII.GetString(clientData, 4, fileNameLen);

                long lunghezzaFile = BitConverter.ToInt32(clientData, 4 + fileNameLen);

                // stampa di debug

                Console.WriteLine("Client:{0} connesso & File {1} inizio ricezione.", s.RemoteEndPoint, fileName);
                
                
                string file = NomeFileDaPath(fileName);

                using (BinaryWriter bWrite = new BinaryWriter(File.Open(path + file, FileMode.Append)))
                {
                    bWrite.Write(clientData, 12 + fileNameLen, receivedBytesLen - 8 - fileNameLen - 4);

                    int letti = receivedBytesLen - 12 - fileNameLen;
                    lunghezzaFile -= letti;
                    while (lunghezzaFile > 0)
                    {
                        letti = s.Receive(clientData);
                        //Thread.Sleep(300);
                        bWrite.Write(clientData, 0, letti);
                        lunghezzaFile -= letti;
                    }


                    Console.WriteLine("File: {0} ricevuto e salvato al path: {1}", file, path);
                }
                    return fileName;

                
            }

            catch (Exception ex)
            {
                Console.WriteLine("Download Fallito : " + ex.Message);
                return null;
            }
        }

        public static void FileUpload(Socket s, string filename,string user)
        {

            byte[] fileData = File.ReadAllBytes(root+user+"\\filesystem\\" + filename);

            byte[] clientData = new byte[fileData.Length];

            fileData.CopyTo(clientData, 0);

            ArrayList DataClient = new ArrayList();

            DataClient = Split(clientData);

            foreach (object o in DataClient)
            {
                s.Send((byte[])o);
                double primo = (double)DataClient.IndexOf(o);
                double secondo = (double)DataClient.Capacity;
                double ttt = primo / secondo;
                ttt *= 100;
                Console.Write("\r Invio di {0} :  {1}%   ", filename, (int)ttt);
            }
            Console.Write("\r Invio di {0} :  {1}%  ==> ", filename, 100);
            Console.WriteLine("Fatto");

            Console.WriteLine("File:{0} è stato inviato ", filename);
        }

        public static void FileUpload(Socket s, string filename, string user,double discriminaOverload)
        {
            try
            {
                byte[] fileNameByte = Encoding.ASCII.GetBytes(NomeFileDaPath(filename, "_"));

                byte[] fileData = File.ReadAllBytes(filename);

                FileInfo fi = new FileInfo(filename);

                /* 4 byte => 1 int per la lunghezza del nome del file +
                 * il numero di byte della lunghezza del nome file +
                 * numero di byte legato alla dimensione dei dati */

                byte[] clientData = new byte[4 + fileNameByte.Length + fileData.Length+8];
                byte[] fileNameLen = BitConverter.GetBytes(fileNameByte.Length);
                byte[] filelen = BitConverter.GetBytes(fi.Length);


                fileNameLen.CopyTo(clientData, 0);
                fileNameByte.CopyTo(clientData, 4);
                filelen.CopyTo(clientData, 4 + fileNameByte.Length);
                fileData.CopyTo(clientData, 4 + fileNameByte.Length + 8);


                ArrayList DataClient = new ArrayList();

                DataClient = Split(clientData);

                foreach (object o in DataClient)
                {
                    s.Send((byte[])o);
                    double primo = (double)DataClient.IndexOf(o);
                    double secondo = (double) DataClient.Capacity;
                    double ttt = primo/secondo;
                    ttt *= 100;
                    Console.Write("\r Invio di {0} :  {1}%   ", filename, (int)ttt);
                }

                Console.Write("\r Invio di {0} :  {1}%  ==> ", filename, 100);
                Console.WriteLine("Fatto");

                Console.WriteLine("File:{0} è stato inviato ", filename);

            }
            catch (Exception e)
            {
                Console.WriteLine("Upload {0} Fallito ", NomeFileDaPath(filename));
                throw e;
            }
        }

        public static void FileUpload(Socket s, string filename, string user,int discriminaOverload)
        {
            try
            {
                byte[] fileNameByte = Encoding.ASCII.GetBytes(NomeFileDaPath(filename,"_"));

                byte[] fileData = File.ReadAllBytes(filename);

              // FileInfo fi = new FileInfo(filename);

                /* 4 byte => 1 int per la lunghezza del nome del file +
                 * il numero di byte della lunghezza del nome file +
                 * numero di byte legato alla dimensione dei dati */

                byte[] clientData = new byte[4 + fileNameByte.Length + fileData.Length];
                byte[] fileNameLen = BitConverter.GetBytes(fileNameByte.Length);
              //  byte[] filelen = BitConverter.GetBytes(fi.Length);


                fileNameLen.CopyTo(clientData, 0);
                fileNameByte.CopyTo(clientData, 4);
              //  filelen.CopyTo(clientData, 4 + fileNameByte.Length);
                fileData.CopyTo(clientData, 4 + fileNameByte.Length/*+8*/);


                ArrayList DataClient = new ArrayList();

                DataClient = Split(clientData);

                foreach (object o in DataClient)
                {
                    s.Send((byte[])o);
                }

                Console.WriteLine("File:{0} è stato inviato ", filename);

            }
            catch (Exception e)
            {
                Console.WriteLine("Upload {0} Fallito ", NomeFileDaPath(filename));
                throw e;
            }

        }

        public static ArrayList Split(byte[] bigfile)
        {
            int chunck = 1024 * 1024;
            int pos = 0;
            int remaining;

            ArrayList result = new ArrayList();

            while ((remaining = bigfile.Length - pos) > 0)
            {
                byte[] block = new byte[Math.Min(remaining, chunck)];

                Array.Copy(bigfile, pos, block, 0, block.Length);
                result.Add(block);

                pos += block.Length;
            }

            return result;
        }

        public static void InviaAck(Socket s)
        {
            byte[] ack = BitConverter.GetBytes(0);
            s.Send(ack);
        }

        public static string AttendiAck(Socket s)
        {
            byte[] buff = new byte[100];
            int ack = s.Receive(buff);
            int fileNameLen;
            if (ack != 0)
            {
                fileNameLen = BitConverter.ToInt32(buff, 0);
                if(fileNameLen==0)
                    return "OK";
            }
            return "ERR";
        }

        public static string NomeFileDaPath(string path)
        {
            try
            {
                char[] delimiterChars = { '\\' };
                string[] words = path.Split(delimiterChars);

                foreach (string s in words)
                {

                    if (s.IndexOf(".") != -1)
                    {
                        return s;
                    }

                }
                return null;
            }
            catch (Exception e)
            {
                Console.WriteLine("Impossibile recuperare il nome file dal path");
                throw e;
            }

        }

        public static string SeparaPathDaMd5(string line)
        {
            char[] delimiterChars = { ':' };
            string[] words = line.Split(delimiterChars);

            return words[0];

        }

        public static string NomeFileDaPath(string path, string del)
        {
            char[] delimiterChars = { '\\' };
            string[] words = path.Split(delimiterChars);

            foreach (string s in words)
            {

                if (s.IndexOf(del) != -1)
                {
                    return s;
                }

            }
            return null;
        }

        public bool reg(Socket clientSock, DBManagement db, SqlConnection sc, String user, String pass)
        {
            try
            {
                if (db.SearchDb(sc, user).Rows.Count == 0)
                {
                    db.InsertDb(sc, user, pass);
                    byte[] ackmsg = Encoding.ASCII.GetBytes("OK");
                    clientSock.Send(ackmsg);
                    Console.WriteLine("Ack inviato al client");
                }

                else
                {
                    Console.WriteLine("Error nome utente già presente");
                    byte[] ackmsg = Encoding.ASCII.GetBytes("ER");
                    clientSock.Send(ackmsg);
                    Console.WriteLine("ERR inviato al client");
                    return false;
                }

                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine("Errore in fase di registrazione di un nuovo utente");
                throw e;
            }
            
        }

        public bool log(Socket clientSock,DBManagement db,SqlConnection sc,String user,String pass)
        {
            try
            {
                if (db.IdentityCheck(sc, user, pass))
                {

                    byte[] ackmsg = Encoding.ASCII.GetBytes("OK");

                    clientSock.Send(ackmsg);

                    Console.WriteLine("Ack inviato al client");
                    return true;
                }

                else
                {
                    Console.WriteLine("Error nome utente errato riprova");
                    byte[] ackmsg = Encoding.ASCII.GetBytes("NO");
                    clientSock.Send(ackmsg);
                }

                return false;
            }

            catch (Exception e)
            {
                Console.WriteLine("Errore in fase di login");
                throw e;
            }

        }

        static void Main(string[] args)
        {
            Program p = new Program();
            //if (Directory.Exists(@"C:\Users\Davide\Documents\progetto malnati\server\root\u3"))
            //    Directory.Delete(@"C:\Users\Davide\Documents\progetto malnati\server\root\u3", true);
            //Directory.CreateDirectory(@"C:\Users\Davide\Documents\progetto malnati\server\root\u3");


            IPAddress[] ipAddress = Dns.GetHostAddresses("192.168.1.109");
            //         IPEndPoint ipEnd = new IPEndPoint(IPAddress.Any, 5656);
            IPEndPoint ipEnd = new IPEndPoint(ipAddress[0], 5656);

            Socket sock = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.IP);
            sock.Bind(ipEnd);
            sock.Listen(100);
            //clientSock is the socket object of client, so we can use it now to transfer data to client
            //Console.WriteLine(" attendo un nuovo client ");


            Console.WriteLine("Server in ascolto sulla porta 5656: ");

            while (true)
            {

                Console.WriteLine("Attesa di connessione.....");

                Socket clientSock = sock.Accept();
                Console.WriteLine("Stabilita una connessione con " + clientSock.RemoteEndPoint);
                try
                {
                    ThreadPool.QueueUserWorkItem(new WaitCallback(p.run), clientSock);
                }
                catch (Exception e)
                {
                    Console.WriteLine("Thread Termina a causa di una eccezione : {0} ", e.StackTrace);
                }
                

            }
        }

   }
   
}
