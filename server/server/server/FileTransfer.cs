﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;


/* nella soluzione dovremo inviare un char da 1 byte iniziale 
 * in cui  il client mi comunica se il prossimo oggetto da
 * trasferire è un file (F) o un direttorio (D)*/

namespace server
{
    class FileTransfer
    {
        /*  *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *    
         *  i file specie quelli di grandi dimensioni devono tagliati a strisce         *
         *  prima di essere inviati: la funzione restituisce una arraylist con tutti    *
         *  i chunck del file che si vuole inviare o ricevere                           *   
         *  *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   */

        public static ArrayList Split(byte[] bigfile)
        {
            int chunck = 1024 * 1024;
            int pos = 0;
            int remaining;

            ArrayList result = new ArrayList();

            while ((remaining = bigfile.Length - pos) > 0)
            {
                byte[] block = new byte[Math.Min(remaining, chunck)];

                Array.Copy(bigfile, pos, block, 0, block.Length);
                result.Add(block);

                pos += block.Length;
            }

            return result;
        }

        /*  *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *    
         *  la funzione si occupa di fare il download dal client di un  file. il nome   *
         *  del file deve essere passato nel seguente formato :                         *
         *  path = @"C:\Users\Davide" per  evitare errori di escaping                   *
         *  *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   */

          public static void FileDownload (Socket s,string filename,string path)
          {
              try{

                    byte[] clientData = new byte[1024 * 5000];

                    int receivedBytesLen = s.Receive(clientData);

                  /* prima invio un intero che indica la lunghezza del nome del file */

                    int fileNameLen = BitConverter.ToInt32(clientData, 0);

                  /* poi leggo il nome del file e lo salvo in una stringa */

                    string fileName = Encoding.ASCII.GetString(clientData, 4, fileNameLen);

                  // stampa di debug

                    Console.WriteLine("Client:{0} connesso & File {1} inizio ricezione.", s.RemoteEndPoint, fileName);


                    BinaryWriter bWrite = new BinaryWriter(File.Open(path + fileName, FileMode.Append)); ;
                    bWrite.Write(clientData, 4 + fileNameLen, receivedBytesLen - 4 - fileNameLen);

                    while (receivedBytesLen > 0)
                    {
                        receivedBytesLen = s.Receive(clientData);
                        bWrite.Write(clientData, 0 , receivedBytesLen);
                  
                    }
               

                    Console.WriteLine("File: {0} ricevuto e salvato al path: {1}", fileName, path);

                    bWrite.Close();


              }

              catch(Exception ex){
                  Console.WriteLine("Download Fallito : " + ex.Message);
              }
          }


          /*  *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *    
           *  la funzione si occupa di fare l'upload al client di un  file. il nome       *
           *  del file deve essere passato nel seguente formato :                         *
           *  path = @"C:\Users\Davide" per  evitare errori di escaping                   *
           *  *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   */


          public static void FileUpload(Socket s, string filename, string path)
          {
              byte[] fileNameByte = Encoding.ASCII.GetBytes(filename);

              byte[] fileData = File.ReadAllBytes(path + filename);
              
              /* 4 byte => 1 int per la lunghezza del nome del file +
               * il numero di byte della lunghezza del nome file +
               * numero di byte legato alla dimensione dei dati */

              byte[] clientData = new byte[4 + fileNameByte.Length + fileData.Length];
              byte[] fileNameLen = BitConverter.GetBytes(fileNameByte.Length);

              fileNameLen.CopyTo(clientData, 0);
              fileNameByte.CopyTo(clientData, 4);
              fileData.CopyTo(clientData, 4 + fileNameByte.Length);


              ArrayList DataClient = new ArrayList();

              DataClient = Split(clientData);

              foreach (object o in DataClient)
              {
                  s.Send((byte[])o);
              }

              Console.WriteLine("File:{0} è stato inviato ", filename);

          }


        /* invio la lunghezza del nome e il nome del direttorio */

          public static void DirUpload(Socket s, string dirname)
          {
              byte[] dirNameByte = Encoding.ASCII.GetBytes(dirname);
              byte[] clientData = new byte[4 + dirNameByte.Length];
              byte[] dirNameLen = BitConverter.GetBytes(dirNameByte.Length);

              dirNameLen.CopyTo(clientData, 0);
              dirNameByte.CopyTo(clientData, 4);

              s.Send(clientData);
 
              Console.WriteLine("Directory :{0} è stato inviato ", dirname);

          }

        /* ricevo il nome del direttorio da creare sul server */
        
          public static void DirDownload(Socket s, string dirname, string path)
          {
              byte[] clientData = new byte[1024];
              int receivedBytesLen = s.Receive(clientData);
              int dirNameLen = BitConverter.ToInt32(clientData, 0);
              string dirName = Encoding.ASCII.GetString(clientData, 4, dirNameLen);

              Directory.CreateDirectory(path+dirname);

              Console.WriteLine("Directory: {0} creata al path: {1}", dirName, path);

          }


        /* salvo su server la cartella di backup con tutti i sotto direttori */

          public static void DirDownloadRecursive(Socket s, string dirname, string path)
          {


          }


        /* invio al client una cartella e tutto il suo contenuto */

        //  public static void DirUploadRecursive(Socket s, string dirname, string path)
        //  {
        //      foreach (string f in Directory.GetFiles(dirname))
        //      {
        //          FileUpload(s, f, path + "\\" + dirname);
        //      }

        //      DirRecursiveFileUpload(s, path, dirname);
        //  }

        ///* funzione di visita ricorsiva dei sottodirettori */

        //  static void DirRecursiveFileUpload(Socket s,string path,string sDir)
        //  {
        //      try
        //      {

        //          foreach (string d in Directory.GetDirectories(sDir))
        //          {
        //              foreach (string f in Directory.GetFiles(d))
        //              {
        //                  FileUpload(s, f, path + "\\" + d);
        //              }
        //              DirUpload(s, d);
        //              DirRecursiveFileUpload(s, path + "\\" + d,d);
        //          }
        //      }
        //      catch (System.Exception excpt)
        //      {
        //          Console.WriteLine("Errore nella ricerca ricorsiva : " + excpt.Message);
        //      }
        //  }




          public void run()
          {
           
          
          }



    }
}
