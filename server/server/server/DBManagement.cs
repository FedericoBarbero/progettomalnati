﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
namespace server
{
    class DBManagement
    {
        public SqlConnection ConnectDb()
        {

            SqlConnection myConnection = new SqlConnection(@"Data Source=(LocalDB)\v11.0;AttachDbFilename='C:\Users\Davide\Documents\progetto malnati\server\server\server\Authdb.mdf';Integrated Security=True;Connect Timeout=30");

            try
            {
                myConnection.Open();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                Console.WriteLine("Failure opening the connection ");
                myConnection = null;
            }

            return myConnection;
        }

        /* cerco all'interno del DB se uno username è già presente */

        public DataTable SearchDb(SqlConnection conn, string user)
        {           
            
            SqlCommand myCommand = new SqlCommand("SELECT * FROM auth WHERE username = '" + user + "' ");

            myCommand.Connection = conn;

            SqlDataAdapter da = new SqlDataAdapter(myCommand);

            DataTable dt = new DataTable();
            try
            {
                da.Fill(dt);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
                Console.WriteLine("Error retrieving data ");
                dt = null;
            }

            return dt;

        }

        /* in fase di login verifico che l'utente che tenta di accedere al
         * servizio sia effettivamente registrato e le credenziali siano corrette */

        public bool IdentityCheck(SqlConnection conn, string user, string pass)
        {
            SqlCommand myCommand = new SqlCommand("SELECT COUNT(*) AS num FROM auth WHERE username = '" + user + "' AND password = '" +pass+  "'" );

            myCommand.Connection = conn;

            SqlDataAdapter da = new SqlDataAdapter(myCommand);

            DataTable dt = new DataTable();

            try
            {
                da.Fill(dt);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
                Console.WriteLine("Error retrieving data ");
                return false;
            }
            
            //int check =user.CompareTo(dt.Rows[0]["Username"].ToString());
            //if (check != 0) return false;
            //check = pass.CompareTo(dt.Rows[0]["Password"].ToString());
            //if (check != 0) return false;

            if (Convert.ToInt32(dt.Rows[0]["num"].ToString()) == 0) return false;

            return true;
        }

        /* in fase di registrazione crea un nuovo utente del servizio*/

        public void InsertDb(SqlConnection conn,string user,string pass)
        {
            SqlCommand myCommand = new SqlCommand("INSERT INTO auth (Username,Password) VALUES ('"+user+"','"+pass+"') ");

            myCommand.Connection = conn;

            myCommand.ExecuteNonQuery();

        }

        public void CloseDB(SqlConnection conn)
        {
            try
            {
                conn.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                Console.WriteLine("Error closing the connection ");
            }

            return;

        }


        public void run()
        {
            DBManagement db = new DBManagement();
            SqlConnection sc;
            //DataTable dt;
            sc = db.ConnectDb();
            //dt = db.SearchDb(sc,"mario");
            //db.InsertDb(sc, "u1", "p1");
            //dt = db.SearchDb(sc, "u1");


            //Console.WriteLine("{0} ", dt.Rows[0]["Username"].ToString());
            //Console.WriteLine("{0} ", dt.Rows[0]["Password"].ToString());


            bool prova = db.IdentityCheck(sc, "mario", "rossi");

            if (prova) Console.WriteLine("OK utente verificato");
            else Console.WriteLine("ERR utente non verificato");

            db.CloseDB(sc);

        }

    }
}
