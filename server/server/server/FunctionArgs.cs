﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace server
{

    /* classe da serializzare/deserializzare per il passaggio/gestione delle credenziali */

    public class FunctionArgs
    {
        public Socket sock {get;set;}
        public string user { get; set; }

    }

}
