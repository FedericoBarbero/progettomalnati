﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.WindowsAPICodePack.Dialogs;
using System.IO;
using System.Threading;
using System.Net.Sockets;
using System.Windows.Media.Animation;
using System.Windows.Threading;

namespace client
{
    /// <summary>
    /// Logica di interazione per BackUp.xaml
    /// </summary>
    public partial class BackUp : Page
    {
        private static string credpath = @"C:\Users\Federico\Documents\progetto malnati\client\client\client\tmp\cred";
        //private static ProgressBar progbar;
        private static Dispatcher wd;

        public BackUp()
        {
            InitializeComponent();
            //bakSent = true;
            var appreference = App.Current as App;
            UserLabel.Content = appreference.user;
            BackButton.IsEnabled = false;
            wd = this.Dispatcher;
            if (!appreference.firstBackUp)
            {
                restoreStatus();
                BackButton.IsEnabled = true;
            }
            
        }

        private void restoreStatus()
        {           
            var appreference = App.Current as App;
            //if (appreference.monServiceOn)
            if(appreference.monServiceOn)
            {
                FolderTextBox.Text = appreference.RootFolderPath;
                ServiceLabel.Visibility = System.Windows.Visibility.Hidden;
                ServiceLabel.Text = "Service Started";
                ServiceLabel.Foreground = new SolidColorBrush(Colors.Green);
                ServiceLabel.Visibility = System.Windows.Visibility.Visible;

                BrowseButton.IsEnabled = false;
                FolderTextBox.IsEnabled = false;
                StartButton.IsEnabled = false;
                StopButton.IsEnabled = true;
            }
            else
            {
                ServiceLabel.Visibility = System.Windows.Visibility.Hidden;
                ServiceLabel.Text = "Service Stopped";
                ServiceLabel.Foreground = new SolidColorBrush(Colors.Red);
                ServiceLabel.Visibility = System.Windows.Visibility.Visible;

                FolderTextBox.Text = appreference.RootFolderPath;
               BrowseButton.IsEnabled = false;
               FolderTextBox.IsEnabled = false;
                StartButton.IsEnabled = true;
                StopButton.IsEnabled = false;
            }

            
        }

        private void BrowseButtonOnClick(object sender, RoutedEventArgs e)
        {
            var dlg = new CommonOpenFileDialog();
            dlg.Title = "My Title";
            dlg.IsFolderPicker = true;
            var currentDirectory = @"C:\Users\Federico\Desktop\";
            dlg.InitialDirectory = currentDirectory;

            dlg.AddToMostRecentlyUsedList = false;
            dlg.AllowNonFileSystemItems = false;
            dlg.DefaultDirectory = currentDirectory;
            dlg.EnsureFileExists = true;
            dlg.EnsurePathExists = true;
            dlg.EnsureReadOnly = false;
            dlg.EnsureValidNames = true;
            dlg.Multiselect = false;
            dlg.ShowPlacesList = true;

            if (dlg.ShowDialog() == CommonFileDialogResult.Ok)
            {
                var folder = dlg.FileName;
                FolderTextBox.Text = folder;

                if(folder.Equals(""))
                {
                    MessageBox.Show("Please select a valid folder!");
                    return;
                }
                var appreference = App.Current as App;
                appreference.RootFolderPath = folder;
                DirectoryInfo info = new DirectoryInfo(folder);
                //appreference.ParentFolder = info.Parent.ToString();            
                appreference.RootFolder = info.Name;

                using (StreamWriter sw = new StreamWriter(new FileStream(credpath + "\\" + appreference.user + ".txt", FileMode.Create)))
                {
                    sw.WriteLine(folder);
                    sw.WriteLine(info.Name);

                }

            }
            else
            {
                MessageBox.Show("Error in Folder Browse Dialog");
            }
        }

        private void StartButtonOnClick(object sender, RoutedEventArgs e)
        {
            var appreference = App.Current as App;
            if (appreference.firstBackUp)
            {
                var folderPath = FolderTextBox.Text;
                if (String.IsNullOrEmpty(folderPath))
                {
                    MessageBox.Show("Please select the proper folder");
                    return;
                }
                ServiceLabel.Visibility = System.Windows.Visibility.Hidden;
                ServiceLabel.Text = "Service Started";
                ServiceLabel.Foreground = new SolidColorBrush(Colors.Green);
                ServiceLabel.Visibility = System.Windows.Visibility.Visible;

                BrowseButton.IsEnabled = false;
                FolderTextBox.IsEnabled = false;
                StartButton.IsEnabled = false;
                StopButton.IsEnabled = true;               
                //launch thread
                appreference.monServiceOn = true;
                ThreadPool.QueueUserWorkItem(new WaitCallback(logFileWaitCallback), appreference.connectedSocket);
                appreference.firstBackUp = false;
                Sincronization.startMonitoring();
                Sincronization.startSincronize(); 
            }
            else
            {
                ServiceLabel.Visibility = System.Windows.Visibility.Hidden;
                ServiceLabel.Text = "Service Started";
                ServiceLabel.Foreground = new SolidColorBrush(Colors.Green);
                ServiceLabel.Visibility = System.Windows.Visibility.Visible;

                BrowseButton.IsEnabled = false;
                FolderTextBox.IsEnabled = false;
                StartButton.IsEnabled = false;
                StopButton.IsEnabled = true;

                appreference.monServiceOn = true;
                Sincronization.startMonitoring();
                Sincronization.startSincronize();
            }
                  
        }

        private void StopButtonOnClick(object sender, RoutedEventArgs e)
        {
            //ServiceLabel.Visibility = System.Windows.Visibility.Hidden;
            //ServiceLabel.Text = "Service Stopped";
            //ServiceLabel.Foreground = new SolidColorBrush(Colors.Red);
            //ServiceLabel.Visibility = System.Windows.Visibility.Visible;

            ////FolderTextBox.Text = "";
            ////BrowseButton.IsEnabled = true;
            ////FolderTextBox.IsEnabled = true;
            //StartButton.IsEnabled = true;
            //StopButton.IsEnabled = false;

            //Sincronization.stopMonitoring();
            //Sincronization.stopSincronize();
            //var appreference = App.Current as App;
            //appreference.monServiceOn = false;
            //Console.WriteLine("stopped monitoring");
        }

        private void BackButtonOnClick(object sender, RoutedEventArgs e)
        {
            ServiceLabel.Visibility = System.Windows.Visibility.Hidden;
            ServiceLabel.Text = "Service Stopped";
            ServiceLabel.Foreground = new SolidColorBrush(Colors.Red);
            ServiceLabel.Visibility = System.Windows.Visibility.Visible;

            //FolderTextBox.Text = "";
            //BrowseButton.IsEnabled = true;
            //FolderTextBox.IsEnabled = true;
            StartButton.IsEnabled = true;
            StopButton.IsEnabled = false;
            var appreference = App.Current as App;
            if (appreference.monServiceOn)
            {
                Sincronization.stopMonitoring();
                Sincronization.stopSincronize();
            }
            
            appreference.monServiceOn = false;
            Console.WriteLine("stopped monitoring");
            //var appreference = App.Current as App;
            MessageBox.Show("Attention! If you go back the app will not monitor your folders.\nBe careful not to make any changes or it won't be synced!", "ALERT");
            var MainFrame = appreference.MainFrame as Frame;
            MainFrame.Navigate(new Uri("BackRestore.xaml", UriKind.Relative));
        }

        delegate void update();

        private void logFileWaitCallback(Object socket)
        {
            Socket s = (Socket)socket;
            Utilities.sendLogFile(s);
            Console.WriteLine("Start sending files....");
            try {
                wd.BeginInvoke(new update(() => {                    
                    MessageBox.Show("Back up started! It may take some minutes.\n Please wait the confirmation message.", "INFO");
                }), null);
                Backup.uploadFS();
                wd.BeginInvoke(new update(() => {
                    //progbar.Visibility = Visibility.Collapsed;
                    //progbar = null;
                    BackButton.IsEnabled = true;
                    MessageBox.Show("Back up completed correctly");
                }), null);
            }
            catch(Exception ex)
            {
                wd.BeginInvoke(new update(() => {
                    //progbar.Visibility = Visibility.Collapsed;
                    //progbar = null;
                    BackButton.IsEnabled = false; 
                    MessageBox.Show("Back up error! retry!");
                    var appreference = App.Current as App;
                    appreference.firstBackUp = true;
                    appreference.logFilePath = null;
                    try
                    {
                        Sincronization.stopMonitoring();
                        Sincronization.stopSincronize();
                    }
                    catch(Exception ex1)
                    {
                        Console.WriteLine("Error in stop monitoring");
                    }
                    appreference.monServiceOn = false;
                    appreference.RootFolder = null;
                    appreference.RootFolderPath = null;
                    appreference.connectedSocket = null;
                    appreference.user = "";
                    var MainFrame = appreference.MainFrame as Frame;
                    MainFrame.Navigate(new Uri("LogInSignUp.xaml", UriKind.Relative));
                }), null);
            }
            
        }
        
    }
}
