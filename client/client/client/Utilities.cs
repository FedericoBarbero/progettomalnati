﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using client;
using System.Windows;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Cryptography;

namespace client
{
    public class Utilities
    {
        private const string OkMsg = "OK";
        private const string ErrMsg = "ER";
        public const string RegMsg = "reg";
        public const string LogMsg = "log";
        private const string BakMsg = "BAK";
        private const string RestMsg = "RES";
        private const string CntMsg = "CNT";
        private const string AbrMsg = "ABR";       
        private static string path2 = @"C:\Users\Federico\Documents\progetto malnati\client\client\client\tmp";
        private static string metaFileName = @"C:\Users\Federico\Documents\progetto malnati\client\client\client\tmp\meta.txt";
        private static string logFileName = "log.txt";
        private static string ipString = "192.168.1.109";

        public static bool writeCredFile(string FilePath, string usr, string pwd, string type)
        {
            try {
                using (FileStream fs = File.Open(FilePath, FileMode.Create))
                {
                    using (StreamWriter sw = new StreamWriter(fs))
                    {
                        sw.Write(type + "\n" + usr + "\n" + pwd);
                    }
                }
                return true;
            }
            catch(Exception e)
            {
                return false;
            }
        }

        public static bool deleteCredFile(string FilePath)
        {
            try {
                //var fs2 = File.Open(FilePath, FileMode.Create);
                //fs2.Close();
                File.Delete(FilePath);
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message + e.StackTrace);
                throw new Exception("error in deleting file cred!");
                //return false;
            }
        }

        public static bool sendFile(string fileName, string fileCred, Socket serverSocket)
        {
            try {
                // uso il filename con estensione quindi
                //string filePath = @"C:\Users\Davide\Documents\Visual Studio 2013\Projects\file transfer\";//Your File Path;
                byte[] fileNameByte = Encoding.ASCII.GetBytes(fileName);

                byte[] fileData = File.ReadAllBytes(fileCred);
                byte[] clientData = new byte[4 + fileNameByte.Length + fileData.Length];
                byte[] fileNameLen = BitConverter.GetBytes(fileNameByte.Length);


                fileNameLen.CopyTo(clientData, 0);
                fileNameByte.CopyTo(clientData, 4);
                fileData.CopyTo(clientData, 4 + fileNameByte.Length);


                serverSocket.Send(clientData);


                Console.WriteLine("File:{0} has been sent.", fileCred);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public static bool authenticate(string fileCred, string fileName, string username, string password, string type)
        {
            try {
                Utilities.writeCredFile(fileCred, username, password, type);
                IPAddress[] ipAddress = Dns.GetHostAddresses(ipString);

                IPEndPoint ipEnd = new IPEndPoint(ipAddress[0], 5656);
                Socket serverSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.IP);
                try {
                    serverSocket.Connect(ipEnd);
                }
                catch(Exception ex1)
                {
                    Console.WriteLine("Error in connect to socket! " + ex1.Message + ex1.StackTrace);
                    MessageBox.Show("Error! Impossible to reach the server!");
                    throw ex1;
                }

                var appreference = App.Current as App;
                
                if (appreference.connectedSocket == null)
                {
                    appreference.connectedSocket = serverSocket;
                }
                else
                {
                    Console.WriteLine("sock not empty!");
                    //throw new Exception();
                }

                Utilities.sendFile(fileName, fileCred, serverSocket);
                if (Utilities.waitAck(serverSocket))
                {
                    Utilities.deleteCredFile(fileCred);
                    return true;
                }
                Utilities.deleteCredFile(fileCred);
                return false;
            }
            catch(Exception e)
            {
                Console.WriteLine("Error in Connect socket.. " + e.StackTrace);
                return false;
            }            
        }

        private static bool waitAck(Socket serverSocket)
        {
            try
            {
                byte[] msg = new byte[2];
                serverSocket.Receive(msg);
                var str = Encoding.ASCII.GetString(msg);
                Console.WriteLine(str);
                if (str.Equals(OkMsg))
                    return true;
                else
                    return false;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        private static string AttendiAck(Socket s)
        {
            byte[] buff = new byte[100];
            int ack = s.Receive(buff);
            int fileNameLen;
            if (ack != 0)
            {
                fileNameLen = BitConverter.ToInt32(buff, 0);
                if (fileNameLen == 0)
                    return "OK";
            }
            return "ERR";
        }

        internal static void InviaAck(Socket s)
        {
            byte[] ack = BitConverter.GetBytes(0);
            s.Send(ack);
        }

        private static string NomeFileDaPath(string path)
        {
            char[] delimiterChars = { '\\' };
            string[] words = path.Split(delimiterChars);

            foreach (string s in words)
            {

                if (s.IndexOf(".") != -1 && s.CompareTo("") != 0)
                {
                    return s;
                }

            }
            return null;
        }

        public static string createRootPath(string path)
        {
            var substr = path.Split('\\');
            string result = "";
            int c = substr.Length;
            c--;
            foreach(string s in substr)
            {
                c--;
                if (c >= 0)
                    result += s + "\\";
                else
                    break;
            }
            return result;
        }

        //internal static void FileSystemLog(string path, string filesystemstruct, int livello)
        //{

        //    try
        //    {
        //        if (!File.Exists(filesystemstruct))
        //        {
        //            using (StreamWriter sw = File.CreateText(filesystemstruct)) { }
        //        }
        //        if(livello == 0)
        //        {
        //            if (path.CompareTo(path2) != 0)
        //                foreach (string f in Directory.GetFiles(path))
        //                {
        //                    using (StreamWriter sw = File.AppendText(filesystemstruct))
        //                    {
        //                        //sw = File.AppendText(filesystemstruct);
        //                        char[] delimiterChars = { '\\' };
        //                        string[] words = f.Split(delimiterChars);

        //                        string stampa = @"\root";
        //                        int flag = 0;
        //                        var appreference = App.Current as App;

        //                        foreach (string s in words)
        //                        {
        //                            if (flag != 0)
        //                                stampa += "\\" + s;
        //                            if (s.CompareTo(appreference.RootFolder) == 0)
        //                                flag = 1;
        //                        }
        //                        sw.WriteLine(stampa);
        //                    }

        //                }
        //        }
        //        foreach (string d in Directory.GetDirectories(path))
        //        {

        //            if (d.CompareTo(path2) != 0)
        //                foreach (string f in Directory.GetFiles(d))
        //                {
        //                    using (StreamWriter sw = File.AppendText(filesystemstruct))
        //                    {
        //                        //sw = File.AppendText(filesystemstruct);
        //                        char[] delimiterChars = { '\\' };
        //                        string[] words = f.Split(delimiterChars);

        //                        string stampa = @"\root";
        //                        int flag = 0;
        //                        var appreference = App.Current as App;

        //                        foreach (string s in words)
        //                        {
        //                            if (flag != 0)
        //                                stampa += "\\" + s;
        //                            if (s.CompareTo(appreference.RootFolder) == 0)
        //                                flag = 1;
        //                        }
        //                        sw.WriteLine(stampa);
        //                    }                            

        //                }

        //            FileSystemLog(d, filesystemstruct, ++livello);
        //        }
        //    }
        //    catch (System.Exception excpt)
        //    {
        //        Console.WriteLine("Errore nella scrittura ricorsiva : " + excpt.Message);
        //    }

        //}

        private static ArrayList Split(byte[] filebytes)
        {
            int range = 1024 * 1024;
            int pos = 0;
            int remaining;

            ArrayList result = new ArrayList();

            while ((remaining = filebytes.Length - pos) > 0)
            {
                byte[] block = new byte[Math.Min(remaining, range)];

                Array.Copy(filebytes, pos, block, 0, block.Length);
                result.Add(block);

                pos += block.Length;
            }

            return result;
        }

        /* filename log.txt path tutto il percorso del file (possibile modifica) */

        public static void FileUpload(Socket s, string filename, string path)
        {
            byte[] fileNameByte = Encoding.ASCII.GetBytes(filename);

            byte[] fileData = File.ReadAllBytes(path);

            FileInfo f = new FileInfo(path); // per trovare la lunghezza in byte del file

            /* 4 byte => 1 int per la lunghezza del nome del file +
             * il numero di byte della lunghezza del nome file +
             * numero di byte legato alla dimensione dei dati */
            byte[] fileLen = BitConverter.GetBytes(f.Length);
            byte[] clientData = new byte[4 + fileNameByte.Length + 8 + fileData.Length];
            byte[] fileNameLen = BitConverter.GetBytes(fileNameByte.Length);

            fileNameLen.CopyTo(clientData, 0);
            fileNameByte.CopyTo(clientData, 4);
            fileLen.CopyTo(clientData, 4 + fileNameByte.Length);
            fileData.CopyTo(clientData, 4 + fileNameByte.Length + 8);


            ArrayList DataClient = new ArrayList();

            DataClient = Split(clientData);

            foreach (object o in DataClient)
            {
                s.Send((byte[])o);
            }

            Console.WriteLine("File:{0} è stato inviato ", filename);

        }

        public static void sendLogFile(Socket s)
        {
            var fileName = Path.GetFileNameWithoutExtension(logFileName);
            DateTime dt = DateTime.Today;

            string nomeattuale = fileName + "_" + dt.Day + +dt.Month + dt.Year + "_" + DateTime.Now.ToString("HH") + DateTime.Now.ToString("mm") + ".txt";
            string filelog = path2 + "\\" + nomeattuale;
           

            //salvo nel contesto dell'applicazione il path del log file corrente
            var appreference = App.Current as App;
            if (File.Exists(appreference.logFilePath))
            {
                File.Delete(appreference.logFilePath);
            }
            //FileSystemLog(appreference.RootFolderPath, filelog, 0);
            FileSystemLogOrdered(appreference.RootFolderPath, filelog);
            
            appreference.logFilePath = filelog;

            try
            {
                // uso il filename con estensione quindi
                //string filePath = @"C:\Users\Davide\Documents\Visual Studio 2013\Projects\file transfer\";//Your File Path;
                byte[] fileNameByte = Encoding.ASCII.GetBytes(nomeattuale);

                byte[] fileData = File.ReadAllBytes(filelog);
                byte[] clientData = new byte[4 + fileNameByte.Length + fileData.Length];
                byte[] fileNameLen = BitConverter.GetBytes(fileNameByte.Length);


                fileNameLen.CopyTo(clientData, 0);
                fileNameByte.CopyTo(clientData, 4);
                fileData.CopyTo(clientData, 4 + fileNameByte.Length);


                ArrayList DataClient = new ArrayList();

                DataClient = Split(clientData);

                foreach (object o in DataClient)
                {
                    s.Send((byte[])o);
                }

                Console.WriteLine("File:{0} has been sent.", fileName);
                //File.Delete(filelog);

            }
            catch (Exception ex)
            {
                Console.WriteLine("Error in sending Log File. " + ex.Message);
                //File.Delete(filelog);
            }
        }

        public static void sendType(int type, Socket s)
        {
            if(type == 0)
            {
                byte[] msg = Encoding.ASCII.GetBytes(BakMsg);
                s.Send(msg);
                return;
            }
            if (type == 1)
            {
                byte[] msg = Encoding.ASCII.GetBytes(RestMsg);
                s.Send(msg);
                return;
            }
            if (type == 2)
            {
                byte[] msg = Encoding.ASCII.GetBytes(CntMsg);
                s.Send(msg);
                return;
            }
            if (type == 3)
            {
                byte[] msg = Encoding.ASCII.GetBytes(AbrMsg);
                s.Send(msg);
                return;
            }
            else
                throw new Exception("Wrong type in backup restore message");
        }

        internal static void sendMetaFile(Socket s, string file)
        {
            FileInfo fi = new FileInfo(file);
            using (StreamWriter sw = new StreamWriter(new FileStream(metaFileName, FileMode.Create)))
            {
                try {
                    //FileFullName
                    sw.WriteLine("FileFullName|" + fi.FullName);
                    //FileName
                    sw.WriteLine("FileName|" + fi.Name);
                    //Length
                    sw.WriteLine("FileLength|" + fi.Length.ToString());
                    //Creation
                    sw.WriteLine("FileCreationTime_Year|" + fi.CreationTime.Year.ToString());
                    sw.WriteLine("FileCreationTime_Month|" + fi.CreationTime.Month.ToString());
                    sw.WriteLine("FileCreationTime_Day|" + fi.CreationTime.Day.ToString());
                    sw.WriteLine("FileCreationTime_Hour|" + fi.CreationTime.Hour.ToString());
                    sw.WriteLine("FileCreationTime_Minute|" + fi.CreationTime.Minute.ToString());
                    sw.WriteLine("FileCreationTime_Second|" + fi.CreationTime.Second.ToString());

                    //Creation UTC
                    sw.WriteLine("FileCreationTimeUTC_Year|" + fi.CreationTimeUtc.Year.ToString());
                    sw.WriteLine("FileCreationTimeUTC_Month|" + fi.CreationTimeUtc.Month.ToString());
                    sw.WriteLine("FileCreationTimeUTC_Day|" + fi.CreationTimeUtc.Day.ToString());
                    sw.WriteLine("FileCreationTimeUTC_Hour|" + fi.CreationTimeUtc.Hour.ToString());
                    sw.WriteLine("FileCreationTimeUTC_Minute|" + fi.CreationTimeUtc.Minute.ToString());
                    sw.WriteLine("FileCreationTimeUTC_Second|" + fi.CreationTimeUtc.Second.ToString());
                    //isReadOnly
                    sw.WriteLine("IsReadOnly|" + fi.IsReadOnly.ToString());
                    //LastAccessTime
                    sw.WriteLine("LastAccessTime_Year|" + fi.LastAccessTime.Year.ToString());
                    sw.WriteLine("LastAccessTime_Month|" + fi.LastAccessTime.Month.ToString());
                    sw.WriteLine("LastAccessTime_Day|" + fi.LastAccessTime.Day.ToString());
                    sw.WriteLine("LastAccessTime_Hour|" + fi.LastAccessTime.Hour.ToString());
                    sw.WriteLine("LastAccessTime_Minute|" + fi.LastAccessTime.Minute.ToString());
                    sw.WriteLine("LastAccessTime_Second|" + fi.LastAccessTime.Second.ToString());
                    //LastAccessTimeUtc
                    sw.WriteLine("LastAccessTimeUtc_Year|" + fi.LastAccessTimeUtc.Year.ToString());
                    sw.WriteLine("LastAccessTimeUtc_Month|" + fi.LastAccessTimeUtc.Month.ToString());
                    sw.WriteLine("LastAccessTimeUtc_Day|" + fi.LastAccessTimeUtc.Day.ToString());
                    sw.WriteLine("LastAccessTimeUtc_Hour|" + fi.LastAccessTimeUtc.Hour.ToString());
                    sw.WriteLine("LastAccessTimeUtc_Minute|" + fi.LastAccessTimeUtc.Minute.ToString());
                    sw.WriteLine("LastAccessTimeUtc_Second|" + fi.LastAccessTimeUtc.Second.ToString());
                    //LastWriteTime
                    sw.WriteLine("LastWriteTime_Year|" + fi.LastWriteTime.Year.ToString());
                    sw.WriteLine("LastWriteTime_Month|" + fi.LastWriteTime.Month.ToString());
                    sw.WriteLine("LastWriteTime_Day|" + fi.LastWriteTime.Day.ToString());
                    sw.WriteLine("LastWriteTime_Hour|" + fi.LastWriteTime.Hour.ToString());
                    sw.WriteLine("LastWriteTime_Minute|" + fi.LastWriteTime.Minute.ToString());
                    sw.WriteLine("LastWriteTime_Second|" + fi.LastWriteTime.Second.ToString());
                    //LastWriteTimeUtc
                    sw.WriteLine("LastWriteTimeUtc_Year|" + fi.LastWriteTimeUtc.Year.ToString());
                    sw.WriteLine("LastWriteTimeUtc_Month|" + fi.LastWriteTimeUtc.Month.ToString());
                    sw.WriteLine("LastWriteTimeUtc_Day|" + fi.LastWriteTimeUtc.Day.ToString());
                    sw.WriteLine("LastWriteTimeUtc_Hour|" + fi.LastWriteTimeUtc.Hour.ToString());
                    sw.WriteLine("LastWriteTimeUtc_Minute|" + fi.LastWriteTimeUtc.Minute.ToString());
                    sw.WriteLine("LastWriteTimeUtc_Second|" + fi.LastWriteTimeUtc.Second.ToString());
                }
                catch(Exception ex)
                {
                    Console.WriteLine("Exception in writing metafile!");
                }
            }
            fi = null;
            var nome = NomeFileDaPath(metaFileName);
            try {
                FileUpload(s, nome, metaFileName);
                Console.WriteLine("MetaFile sent!");
            }
            catch(Exception ex)
            {
                Console.WriteLine("FileUploadException! " + ex.Message + " " + ex.StackTrace);
            }

            try
            {
                File.Delete(metaFileName);
                Console.WriteLine("MetaFile deleted!");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Delete Meta File Failed! " + ex.Message + " " + ex.StackTrace);
            }
        }
        
        internal static MyFileInfo recMetaFile(Socket s)
        {
            MyFileInfo mfi = new MyFileInfo();
            string file = null;

            try
            {
                file = FileDownload(s, path2);
            }
            catch (Exception ex)
            {
                Console.WriteLine("FileDownloadException! " + ex.Message + " " + ex.StackTrace);
                throw new Exception("file download error!");
            }

            using (StreamReader sr = new StreamReader(new FileStream(path2 + "\\" + file, FileMode.Open)))
            {
                string line = null;
                string[] fields = new string[40];
                int i = 0;
                while((line = sr.ReadLine())!=null)
                {
                    try
                    {
                        char[] delimiters = { '|' };
                        var res = line.Split(delimiters);
                        fields[i++] = res[1];
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("error in saving metadata" + ex.Message + ex.StackTrace);
                    }
                }
                i = 0;

                mfi.FullName = fields[i++];
                mfi.Name = fields[i++];
                mfi.Length = Convert.ToInt64(fields[i++]);
                mfi.setCreationTime(Convert.ToInt32(fields[i++]), Convert.ToInt32(fields[i++]), Convert.ToInt32(fields[i++]), Convert.ToInt32(fields[i++]), Convert.ToInt32(fields[i++]), Convert.ToInt32(fields[i++]));
                mfi.setCreationTimeUtc(Convert.ToInt32(fields[i++]), Convert.ToInt32(fields[i++]), Convert.ToInt32(fields[i++]), Convert.ToInt32(fields[i++]), Convert.ToInt32(fields[i++]), Convert.ToInt32(fields[i++]));
                mfi.setIsReadOnly(Convert.ToBoolean(fields[i++]));
                mfi.setLastAccessTime(Convert.ToInt32(fields[i++]), Convert.ToInt32(fields[i++]), Convert.ToInt32(fields[i++]), Convert.ToInt32(fields[i++]), Convert.ToInt32(fields[i++]), Convert.ToInt32(fields[i++]));
                mfi.setLastAccessTimeUtc(Convert.ToInt32(fields[i++]), Convert.ToInt32(fields[i++]), Convert.ToInt32(fields[i++]), Convert.ToInt32(fields[i++]), Convert.ToInt32(fields[i++]), Convert.ToInt32(fields[i++]));
                mfi.setLastWriteTime(Convert.ToInt32(fields[i++]), Convert.ToInt32(fields[i++]), Convert.ToInt32(fields[i++]), Convert.ToInt32(fields[i++]), Convert.ToInt32(fields[i++]), Convert.ToInt32(fields[i++]));
                mfi.setLastWriteTimeUtc(Convert.ToInt32(fields[i++]), Convert.ToInt32(fields[i++]), Convert.ToInt32(fields[i++]), Convert.ToInt32(fields[i++]), Convert.ToInt32(fields[i++]), Convert.ToInt32(fields[i++]));
             }
            try
            {
                File.Delete(path2 + "\\" + file);
            }
            catch(Exception ex)
            {
                Console.WriteLine("Error! Impossible to delete meta file received! " + ex.Message +ex.StackTrace);
            }
            return mfi;
        }

        /*public static void sendMetaSerialized(Socket s, string file)
        {

            FileInfo fi = new FileInfo(file);
            Console.WriteLine("File info received sendMetaSerialized: " + fi.FullName + fi.LastWriteTime);
            BinaryFormatter bf = new BinaryFormatter();
            try {
                bf.AssemblyFormat = System.Runtime.Serialization.Formatters.FormatterAssemblyStyle.Simple;
                NetworkStream stm = new NetworkStream(s);
                bf.Serialize(stm, fi);
            }
            catch (Exception e)
            {
                Console.WriteLine("Error in net ser of file {0} : {1}", fi.Name, e.Message);
            }
            Console.WriteLine("Serialization over the net of file {0} completed.", fi.Name);
        } 
        public static FileInfo recMetaSerialized(Socket s)
        {
            FileInfo o = null;
            try {
                BinaryFormatter bf = new BinaryFormatter();
                NetworkStream stm = new NetworkStream(s);
                o = (FileInfo)bf.Deserialize(stm);
                Console.WriteLine("File info received recMetaSerialized: " + o.FullName + o.LastWriteTime);
                return o;
            }
            catch(Exception e)
            {
                Console.WriteLine("Error in deserialization from the network. " +e.Message + e.StackTrace);
            }
            return null;
        }
        public static void saveMetaSerialized(string file, FileInfo obj)
        {
            if (obj == null)
                throw new Exception("Error! File info obj is null!");
            Console.WriteLine("File info received saveMetaSerialized: " + obj.FullName + obj.LastWriteTime);
            BinaryFormatter bf = new BinaryFormatter();
            try {
                using (FileStream ms = new FileStream(file, FileMode.Create))
                {
                    bf.Serialize(ms, obj);

                }
            }            
            catch (Exception e)
            {
                Console.WriteLine("Error in file ser of file {0} : {1}", obj.Name, e.Message);
            }
            Console.WriteLine("Serialization over the net of file {0} completed.", obj.Name);
        }
        public static FileInfo readMetaSerialized(string file)
        {
            FileInfo o = null;
            try
            {
                BinaryFormatter bf = new BinaryFormatter();
                FileStream stm = new FileStream(file,FileMode.Open);
                o = (FileInfo)bf.Deserialize(stm);
                Console.WriteLine("File info received readMetaSerialized : " + o.FullName + o.LastWriteTime);
                return o;
            }
            catch (Exception e)
            {
                Console.WriteLine("Error in deserialization from the file. " + e.Message + e.StackTrace);
            }
            return null;
        }*/
        public static void generateLogFile()
        {
            var fileName = Path.GetFileNameWithoutExtension(logFileName);
            DateTime dt = DateTime.Today;

            string nomeattuale = fileName + "_" + dt.Day + +dt.Month + dt.Year + "_" + DateTime.Now.ToString("HH") + DateTime.Now.ToString("mm") + ".txt";
            string filelog = path2 + "\\" + nomeattuale;

            var appreference = App.Current as App;

            //FileSystemLog(appreference.RootFolderPath, filelog, 0);
            FileSystemLogOrdered(appreference.RootFolderPath, filelog);
            
            appreference.logFilePath = filelog;
        }
        //public static void generateLogFile(string nomeattuale)
        //{
        //    //var fileName = Path.GetFileNameWithoutExtension(logFileName);
        //    DateTime dt = DateTime.Today;

        //    //string nomeattuale = fileName + "_" + dt.Day + +dt.Month + dt.Year + "_" + DateTime.Now.ToString("HH") + DateTime.Now.ToString("mm") + ".txt";
        //    string filelog = path2 + "\\" + nomeattuale;

        //    var appreference = App.Current as App;

        //    //FileSystemLog(appreference.RootFolderPath, filelog, 0);
        //    FileSystemLogOrdered(appreference.RootFolderPath, filelog);

        //    //appreference.logFilePath = filelog;
        //}
        public static void Empty(string path)
        {
            DirectoryInfo directory = new DirectoryInfo(path);
            foreach (System.IO.FileInfo file in directory.GetFiles())
                try {
                    file.Delete();
                }
                catch(Exception ex)
                {
                    Console.WriteLine("Errore nello svuotamento cartella " + ex.Message + ex.StackTrace);
                }
            //foreach (System.IO.DirectoryInfo subDirectory in directory.GetDirectories())
            //{
            //    if (subDirectory.Name.Equals("cred"))
            //        continue;
            //    else
            //        subDirectory.Delete(true);
            //}
        }
        internal static void FileSystemLog(string path, string filesystemstruct, int livello, Dictionary<string, DateTime> dic)
        {

            try
            {
                //if (!File.Exists(filesystemstruct))
                //{
                //    using (StreamWriter sw = File.CreateText(filesystemstruct)) { }
                //}
                if (livello == 0)
                {
                    if (path.CompareTo(path2) != 0)
                        foreach (string f in Directory.GetFiles(path))
                        {
                            //using (StreamWriter sw = File.AppendText(filesystemstruct))
                            //{
                            //sw = File.AppendText(filesystemstruct);
                            char[] delimiterChars = { '\\' };
                            string[] words = f.Split(delimiterChars);

                            string stampa = @"\root";
                            int flag = 0;
                            var appreference = App.Current as App;

                            foreach (string s in words)
                            {
                                if (flag != 0)
                                    stampa += "\\" + s;
                                if (s.CompareTo(appreference.RootFolder) == 0)
                                    flag = 1;
                            }
                            FileInfo fi = new FileInfo(f);
                            var md5 = generateMD5(f);
                            dic.Add(stampa+":"+md5, fi.LastWriteTime);
                            //    sw.WriteLine(stampa);
                            //}

                        }
                }
                foreach (string d in Directory.GetDirectories(path))
                {

                    if (d.CompareTo(path2) != 0)
                        foreach (string f in Directory.GetFiles(d))
                        {
                            //using (StreamWriter sw = File.AppendText(filesystemstruct))
                            //{
                            //sw = File.AppendText(filesystemstruct);
                            char[] delimiterChars = { '\\' };
                            string[] words = f.Split(delimiterChars);

                            string stampa = @"\root";
                            int flag = 0;
                            var appreference = App.Current as App;

                            foreach (string s in words)
                            {
                                if (flag != 0)
                                    stampa += "\\" + s;
                                if (s.CompareTo(appreference.RootFolder) == 0)
                                    flag = 1;
                            }

                            FileInfo fi = new FileInfo(f);
                            var md5 = generateMD5(f);
                            dic.Add(stampa+":"+md5, fi.LastWriteTime);
                            //    sw.WriteLine(stampa);
                            //}

                        }

                    FileSystemLog(d, filesystemstruct, ++livello, dic);
                }

            }
            catch (System.Exception excpt)
            {
                Console.WriteLine("Errore nella scrittura ricorsiva : " + excpt.Message);
            }

        }
        internal static void FileSystemLogOrdered(string path, string filesystemstruct)
        {
            var appreference = App.Current as App;
            Dictionary<string, DateTime> di = new Dictionary<string, DateTime>();
            FileSystemLog(appreference.RootFolderPath, filesystemstruct, 0, di);
            if (!File.Exists(filesystemstruct))
            {
                using (StreamWriter sw1 = File.CreateText(filesystemstruct)) { }
            }

            using (StreamWriter sw = File.AppendText(filesystemstruct))
            {
                foreach (KeyValuePair<string, DateTime> ff in di.OrderBy(key => key.Value))
                {
                    sw.WriteLine(ff.Key);
                    Console.WriteLine("Key: {0}, Value: {1}", ff.Key, ff.Value);
                }
            }
        }
        internal static string buildRootPath(string line)
        {
            var appreference = App.Current as App;
            //build path to be sent to the server
            char[] delimiterChars = { '\\' };
            string[] words = line.Split(delimiterChars);

            string path = @"\root";
            int flag = 0;

            foreach (string str in words)
            {
                if (flag != 0)
                    path += "\\" + str;
                if (str.CompareTo(appreference.RootFolder) == 0)
                    flag = 1;
            }

            return path;
        }
        internal static string revertRootPath(string line)
        {
            var appreference = App.Current as App;
            //build path to be sent to the server
            char[] delimiterChars = { '\\' };
            string[] words = line.Split(delimiterChars);

            string path = appreference.RootFolderPath;
            int flag = 0;

            foreach (string str in words)
            {
                if (flag != 0)
                    path += "\\" + str;
                if (str.CompareTo(@"root") == 0)
                    flag = 1;
            }
            return path;
        }
        internal static string generateMD5(string filename)
        {
            try {
                using (var md5 = MD5.Create())
                {
                    using (var stream = File.OpenRead(filename))
                    {
                        var data = md5.ComputeHash(stream);
                        // Create a new Stringbuilder to collect the bytes
                        // and create a string.
                        StringBuilder sBuilder = new StringBuilder();

                        // Loop through each byte of the hashed data 
                        // and format each one as a hexadecimal string.
                        for (int i = 0; i < data.Length; i++)
                        {
                            sBuilder.Append(data[i].ToString("x2"));
                        }

                        // Return the hexadecimal string.
                        return sBuilder.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception in md5 create " + ex.Message + ex.StackTrace);
                return null;
            }
        }
        internal static string getPathFromLogLine(string line)
        {
            try {
                char[] delimiters = { ':' };
                var res = line.Split(delimiters);
                return res[0];
            }
            catch(Exception ex)
            {
                Console.WriteLine("error in splitting md5-log" + ex.Message + ex.StackTrace);
                return null;
            }
        }
        internal static string getMD5FromLogLine(string line)
        {
            try
            {
                char[] delimiters = { ':' };
                var res = line.Split(delimiters);
                return res[1];
            }
            catch (Exception ex)
            {
                Console.WriteLine("error in splitting md5-log" + ex.Message + ex.StackTrace);
                return null;
            }
        }        
        public static string FileDownload(Socket s, string path)
        {
            try
            {

                byte[] clientData = new byte[150000];

                int receivedBytesLen = s.Receive(clientData);

                /* prima invio un intero che indica la lunghezza del nome del file */

                int fileNameLen = BitConverter.ToInt32(clientData, 0);

                /* poi leggo il nome del file e lo salvo in una stringa */

                string fileName = Encoding.ASCII.GetString(clientData, 4, fileNameLen);

                long lunghezzaFile = BitConverter.ToInt32(clientData, 4 + fileNameLen);

                // stampa di debug

                Console.WriteLine("Client:{0} connesso & File {1} inizio ricezione.", s.RemoteEndPoint, fileName);

                string file = fileName;
                //string file = NomeFileDaPath(fileName);

                using (BinaryWriter bWrite = new BinaryWriter(File.Open(path + "\\" + file, FileMode.Append)))
                {
                    bWrite.Write(clientData, 12 + fileNameLen, receivedBytesLen - 8 - fileNameLen - 4);

                    int letti = receivedBytesLen - 12 - fileNameLen;
                    lunghezzaFile -= letti;
                    while (lunghezzaFile > 0)
                    {
                        letti = s.Receive(clientData);
                        //Thread.Sleep(300);
                        bWrite.Write(clientData, 0, letti);
                        lunghezzaFile -= letti;
                    }


                    Console.WriteLine("File: {0} ricevuto e salvato al path: {1}", file, path);
                }
                return fileName;


            }

            catch (Exception ex)
            {
                Console.WriteLine("Download Fallito : " + ex.Message);
                return null;
            }
        }

    }
}
