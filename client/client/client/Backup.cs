﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace client
{
    class Backup
    {
        public static void uploadFS()
        {
            string line2, line;
            var appreference = App.Current as App;
            string filelog = appreference.logFilePath;
            Socket clientSock = appreference.connectedSocket;

            using (StreamReader file = new System.IO.StreamReader(filelog))
            {
                string ack_o_err;

                while ((line2 = file.ReadLine()) != null)
                {
                    line = Utilities.getPathFromLogLine(line2);
                    string nome = NomeFileDaPath(line);
                    var fileFullName = appreference.RootFolderPath + subPathFile(line);
                    if (nome != null)
                    {
                        try
                        {
                            FileUpload(clientSock, nome, fileFullName);
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine("Errore nella file Upload nel backup del fs!");
                            throw ex;
                        }
                    }
                    ack_o_err = AttendiAck(clientSock);
                    if (ack_o_err == "ERR")
                    {
                        Console.WriteLine("Errore nel trasferimento di {0} :", nome);
                        return;
                    }
                    if (nome != null)
                        //Utilities.sendMetaSerialized(clientSock, fileFullName);//MetadataUpload(clientSock, fileFullName);
                        Utilities.sendMetaFile(clientSock, fileFullName);
                    ack_o_err = AttendiAck(clientSock);
                    if (ack_o_err == "ERR")
                    {
                        Console.WriteLine("Errore nel trasferimento di {0} :", nome);
                        return;
                    }
                }
            }
            Console.WriteLine("All files correctly sent!");
            //clientSock.Close();
            //Console.ReadLine();
        }

        private static string subPathFile(string path)
        {
            char[] delimiterChars = { '\\' };
            string[] words = path.Split(delimiterChars);
            string result = "";

            for(int i = 2; i<words.Length;i++)
            {
                result += "\\" + words[i];
            }
            return result;
        }

        private static ArrayList Split(byte[] filebytes)
        {
            int range = 1024 * 1024;
            int pos = 0;
            int remaining;

            ArrayList result = new ArrayList();

            while ((remaining = filebytes.Length - pos) > 0)
            {
                byte[] block = new byte[Math.Min(remaining, range)];

                Array.Copy(filebytes, pos, block, 0, block.Length);
                result.Add(block);

                pos += block.Length;
            }

            return result;
        }

        /* filename log.txt path tutto il percorso del file (possibile modifica) */

        public static void FileUpload(Socket s, string filename, string path)
        {
            try {
                byte[] fileNameByte = Encoding.ASCII.GetBytes(filename);

                byte[] fileData = File.ReadAllBytes(path);

                FileInfo f = new FileInfo(path); // per trovare la lunghezza in byte del file

                /* 4 byte => 1 int per la lunghezza del nome del file +
                 * il numero di byte della lunghezza del nome file +
                 * numero di byte legato alla dimensione dei dati */
                byte[] fileLen = BitConverter.GetBytes(f.Length);
                byte[] clientData = new byte[4 + fileNameByte.Length + 8 + fileData.Length];
                byte[] fileNameLen = BitConverter.GetBytes(fileNameByte.Length);

                fileNameLen.CopyTo(clientData, 0);
                fileNameByte.CopyTo(clientData, 4);
                fileLen.CopyTo(clientData, 4 + fileNameByte.Length);
                fileData.CopyTo(clientData, 4 + fileNameByte.Length + 8);


                ArrayList DataClient = new ArrayList();

                DataClient = Split(clientData);
                try
                {
                    foreach (object o in DataClient)
                    {

                        s.Send((byte[])o);
                    }
                }
                catch (SocketException se)
                {
                    Console.WriteLine("Socket Exception!! {0}", se.Message);
                    throw se;
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine("Error in the readBytes from file.. Upload stopped!");
                throw ex;
            }
            Console.WriteLine("File:{0} è stato inviato ", filename);

        }
        private static string AttendiAck(Socket s)
        {
            byte[] buff = new byte[100];
            int ack = -1;
            try {
                ack = s.Receive(buff);
            }
            catch(Exception se)
            {
                Console.WriteLine("AttendiAck exception! : {0}", se.Message);
            }
            int fileNameLen;
            if (ack != 0)
            {
                fileNameLen = BitConverter.ToInt32(buff, 0);
                if (fileNameLen == 0)
                    return "OK";
            }
            return "ERR";
        }

        private static string NomeFileDaPath(string path)
        {
            char[] delimiterChars = { '\\' };
            string[] words = path.Split(delimiterChars);

            foreach (string s in words)
            {

                if (s.IndexOf(".") != -1 && s.CompareTo("") != 0)
                {
                    return s;
                }

            }
            return null;
        }

    }
}