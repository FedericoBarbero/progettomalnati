﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace client
{
    class MyFileInfo
    {
        private DateTime lastWriteTime;
        private DateTime lastWriteTimeUtc;
        private DateTime lastAccessTime;
        private DateTime lastAccessTimeUtc;
        private DateTime creationTime;
        private DateTime creationTimeUtc;
        private bool isReadOnly;
        internal string FullName { get; set; }
        internal string Name { get; set; }
        internal long Length { get; set; }
        
        internal void setLastWriteTime(int Year, int Month, int Day, int Hour, int Minute, int Second)
        {
            lastWriteTime = new DateTime(Year, Month, Day, Hour, Minute, Second);
        }

        internal void setLastWriteTimeUtc(int Year, int Month, int Day, int Hour, int Minute, int Second)
        {
            lastWriteTimeUtc = new DateTime(Year, Month, Day, Hour, Minute, Second);
        }

        internal void setLastAccessTime(int Year, int Month, int Day, int Hour, int Minute, int Second)
        {
            lastAccessTime = new DateTime(Year, Month, Day, Hour, Minute, Second);
        }

        internal void setLastAccessTimeUtc(int Year, int Month, int Day, int Hour, int Minute, int Second)
        {
            lastAccessTimeUtc = new DateTime(Year, Month, Day, Hour, Minute, Second);
        }

        internal void setCreationTime(int Year, int Month, int Day, int Hour, int Minute, int Second)
        {
            creationTime = new DateTime(Year, Month, Day, Hour, Minute, Second);
        }

        internal void setCreationTimeUtc(int Year, int Month, int Day, int Hour, int Minute, int Second)
        {
            creationTimeUtc = new DateTime(Year, Month, Day, Hour, Minute, Second);
        }

        internal void setIsReadOnly(bool value)
        {
            isReadOnly = value;
        }

        internal DateTime getLastWriteTime()
        {
            return lastWriteTime;
        }

        internal DateTime getLastWriteTimeUtc()
        {
            return lastWriteTimeUtc;
        }

        internal DateTime getLastAccessTime()
        {
            return lastAccessTime;
        }

        internal DateTime getLastAccessTimeUtc()
        {
            return lastAccessTimeUtc;
        }

        internal DateTime getcreationTime()
        {
            return creationTime;
        }

        internal DateTime getcreationTimeUtc()
        {
            return creationTimeUtc;
        }

        internal bool getIsReadOnly()
        {
            return isReadOnly;
        }
    }
}
