﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace client
{
    /// <summary>
    /// Logica di interazione per App.xaml
    /// </summary>
    public partial class App : Application
    {
        public Frame MainFrame;
        public string user = "";
        public string RootFolderPath { get; set; }
        public string RootFolder { get; set; }
        public Socket connectedSocket = null;
        internal bool firstBackUp = false;
        //internal string ParentFolder { get; set; }
        internal string logFilePath = "";
        internal bool monitoring = false;
        internal bool monServiceOn = false;
        internal bool sincronizing = false;
        internal Mutex msincro = new Mutex();
        //public string RootFolderPath = @"C:\Users\Federico\Desktop\NuovaCartella";
        //public string RootFolder = "NuovaCartella";
    }
}
