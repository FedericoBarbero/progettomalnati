﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net;

namespace client
{
    /// <summary>
    /// Logica di interazione per MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            var appreference = App.Current as App;
            appreference.MainFrame = this.MainFrame;
            MainFrame.Navigate(new Uri("LogInSignUp.xaml", UriKind.Relative));
            //MainFrame.Navigate(new Uri("Restore.xaml", UriKind.Relative));
        }

        
    }
}
