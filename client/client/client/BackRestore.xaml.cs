﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace client
{
    /// <summary>
    /// Logica di interazione per Page2.xaml
    /// </summary>
    public partial class Page2 : Page
    {
        private string tmpPath = @"C:\Users\Federico\Documents\progetto malnati\client\client\client\tmp";

        public Page2()
        {
            InitializeComponent();
            var appreference = App.Current as App;
            UserLabel.Content = appreference.user;
            if(appreference.firstBackUp)
            {
                RestoreRadioButton.IsEnabled = false;
            }
        }

        private void ExitButtonOnClick(object sender, RoutedEventArgs e)
        {
            //Navigate back to authentication and clear app context

            var appreference = App.Current as App;
            appreference.user = "";
            if (appreference.logFilePath != "")
            {
                try
                {
                    File.Delete(appreference.logFilePath);
                    appreference.logFilePath = "";
                }
                catch (Exception ex)
                {
                    //MessageBox.Show("Error in deleting log file " +ex.Message +ex.StackTrace);
                    Console.WriteLine("Error in deleting log file " + ex.Message + ex.StackTrace);
                }
            }
            if(appreference.monitoring == true)
            {
                Sincronization.stopMonitoring();
                Sincronization.stopSincronize();                
            }
            appreference.monitoring = false;
            appreference.monServiceOn = false;
            appreference.firstBackUp = false;
            appreference.sincronizing = false;
            appreference.connectedSocket.Close();
            appreference.connectedSocket = null;
            //appreference.ParentFolder = "";
            appreference.RootFolder = "";
            appreference.RootFolderPath = "";
            Utilities.Empty(tmpPath);
            var MainFrame = appreference.MainFrame as Frame;
            MainFrame.Navigate(new Uri("LogInSignUp.xaml", UriKind.Relative));
        }

        private void ContinueButtonOnClick(object sender, RoutedEventArgs e)
        {
            if(BackUpRadioButton.IsChecked == true)
            {
                //Send back up msg
                var appreference = App.Current as App;
                if(appreference.firstBackUp)
                    Utilities.sendType(0, appreference.connectedSocket);
                //MessageBox.Show("message sent");
                var MainFrame = appreference.MainFrame as Frame;
                MainFrame.Navigate(new Uri("BackUp.xaml", UriKind.Relative));
            }
            else if(RestoreRadioButton.IsChecked == true)
            {
                //Send restore up msg
                var appreference = App.Current as App;
                if(appreference.logFilePath.Equals(""))
                {
                    Utilities.generateLogFile();
                }
                //Utilities.sendType(1, appreference.connectedSocket);
                //MessageBox.Show("message sent");
                var MainFrame = appreference.MainFrame as Frame;
                MainFrame.Navigate(new Uri("Restore.xaml", UriKind.Relative));
            }
            else
            {
                MessageBox.Show("Please select one of the two options");
            }
        }
    }
}
