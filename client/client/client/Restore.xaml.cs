﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace client
{
    /// <summary>
    /// Logica di interazione per Page3.xaml
    /// </summary>
    public partial class Page3 : Page
    {
        private App appreference = App.Current as App;
        private FileItem selected;
        private string file;
        private List<MyFileInfo> versions;
        private bool first = true;
        private string versionSelected;
        private bool end = false;
        private delegate void update();
        private Dispatcher wd;

        public Page3()
        {
            InitializeComponent();
            wd = this.Dispatcher;
            //send BAK message
            Utilities.sendType(1, appreference.connectedSocket);
            UserLabel.Content = appreference.user;
            var itemProvider = new ItemProvider();
            var items = itemProvider.GetItems(appreference.RootFolderPath);
            DataContext = items;
            Sincronization.stopMonitoring();
            appreference.monServiceOn = false;
            //Sincronization.stopSincronize();
        }
        private void RestoreButtonOnClick(object sender, RoutedEventArgs e)
        {
            appreference.msincro.WaitOne();
            while(appreference.sincronizing == true)
            {
                appreference.msincro.ReleaseMutex();
                Thread.Sleep(500);
                appreference.msincro.WaitOne();
            }
            appreference.msincro.ReleaseMutex();

            var s = appreference.connectedSocket;
            if (first)
            {
                if (selected == null)
                {
                    MessageBox.Show("Please select a valid file!");
                    return;
                }
                Utilities.sendType(2, s);
                BackButton.IsEnabled = false;
                file = Utilities.buildRootPath(selected.Path);
                byte[] fileToSend = Encoding.ASCII.GetBytes(file);
                try
                {
                    s.Send(fileToSend);
                    //Receive a number
                    byte[] number = new byte[4];
                    s.Receive(number);

                    var i = BitConverter.ToInt32(number, 0);
                    if(i==0)
                    {
                        MessageBox.Show("No previous versions available for the selected file!\nPlease select another file!","ALERT");
                        selected = null;
                        file = null;
                        var MainFrame = appreference.MainFrame as Frame;
                        MainFrame.Navigate(new Uri("BackRestore.xaml", UriKind.Relative));
                        //throw new Exception("Wrong number of version received!");//return;
                    }
                    versions = new List<MyFileInfo>();

                    for (int j = 0; j < i; j++)
                    {
                        //var res = Utilities.recMetaSerialized(s);
                        var res = Utilities.recMetaFile(s);
                        Console.WriteLine("MetaReceived {0}: " + res.FullName + " " + res.getLastWriteTime(), j);
                        versions.Add(res);
                    }

                    //Hide TreeView and show List
                    treeView.Visibility = Visibility.Hidden;
                    Label.Content = selected.Name;
                    List<string> vdates = new List<string>();
                    for (int j = 0; j < versions.Count; j++)
                        vdates.Add(versions[j].getLastWriteTime().ToString());
                    ListBox.ItemsSource = vdates;
                    VersionGrid.Visibility = Visibility.Visible;
                    BackButton.IsEnabled = false;
                    first = false;

                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error in backup: " + ex.Message + ex.StackTrace);
                }
            }
            else
            {
                
                if(versionSelected == null || versionSelected.Equals(String.Empty))
                {
                    MessageBox.Show("Please select a version to restore");
                    return;
                }
                int i = 0;
                for (i = 0; i < versions.Count; i++)
                {
                    if (versions[i].getLastWriteTime().ToString().Equals(versionSelected))
                    {
                        i++;
                        break;
                    }
                }

                //send the integer
                byte[] integerToSend = BitConverter.GetBytes(i);
                s.Send(integerToSend);
                RestoreButton.IsEnabled = false;
                //receive the file 
                ThreadPool.QueueUserWorkItem(new WaitCallback(restoreCallback),i);
                MessageBox.Show("The file restore is in progress. Please notice that it may take a while depending on the file size.\nPlease be patient!","IMPORTANT");           
            }
        }

        private void restoreCallback(object o)
        {
            int i = (int)o;
            var appreference = App.Current as App;
            Socket s = appreference.connectedSocket;
            //receive the file
            try
            {
                File.Delete(selected.Path);
            }
            catch(Exception ex)
            {
                Console.WriteLine("Error in deleting the file before restoring a previous version: " + ex.Message + " " + ex.StackTrace);
                throw ex;
            }
            FileDownload(s, selected.Path, versions[i - 1].Length);
            Utilities.InviaAck(s);
            try
            {
                MyFileInfo toBeRestored = versions[i - 1];
                FileInfo toRestore = new FileInfo(selected.Path);
                //toRestore.Attributes = toBeRestored.Attributes;
                toRestore.CreationTime = toBeRestored.getcreationTime();
                toRestore.CreationTimeUtc = toBeRestored.getcreationTimeUtc();
                toRestore.IsReadOnly = toBeRestored.getIsReadOnly();
                toRestore.LastAccessTime = toBeRestored.getLastAccessTime();
                toRestore.LastAccessTimeUtc = toBeRestored.getLastAccessTimeUtc();
                toRestore.LastWriteTime = toBeRestored.getLastWriteTime();
                toRestore.LastWriteTimeUtc = toBeRestored.getLastWriteTimeUtc();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error in overwriting metadata! " + ex.Message + ex.StackTrace);
                throw ex;
            }

            wd.BeginInvoke(new update(() =>
            {
                MessageBox.Show("File correctly restored!");
                //restore status
                treeView.Visibility = Visibility.Visible;
                Label.Content = "";
                ListBox.ItemsSource = null;
                VersionGrid.Visibility = Visibility.Hidden;
                BackButton.IsEnabled = true;
                RestoreButton.IsEnabled = true;

                first = true;
                selected = null;
                file = "";
                versions = null;
                versionSelected = "";
                BackButton.IsEnabled = true;
                end = true;
                //Navigate back
                if (appreference.monitoring == true)
                { 
                    Sincronization.startMonitoring();
                    appreference.monServiceOn = true;
                }
                var MainFrame = appreference.MainFrame as Frame;                
                MainFrame.Navigate(new Uri("BackRestore.xaml", UriKind.Relative));
            }));
        }

        private void backButtonOnClick(object sender, RoutedEventArgs e)
        {

            //selected = null;
            //file = null;
            //versions = null;
            if(!end)
                Utilities.sendType(3, appreference.connectedSocket);
            end = false;
            Sincronization.startMonitoring();           
            //Sincronization.startSincronize();
            var MainFrame = appreference.MainFrame as Frame;
            appreference.monServiceOn = true;
            MainFrame.Navigate(new Uri("BackRestore.xaml", UriKind.Relative));
        }

        private void onSelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            Item send = (Item)((TreeView)sender).SelectedItem;
            if (send.GetType() == typeof(DirectoryItem))
            {
                selected = null;
                return;
            }
            selected = (FileItem)send;
        }

        private void onVersionSelectedChanged(object sender, SelectionChangedEventArgs e)
        {
            if (ListBox.ItemsSource == null || ListBox.SelectedItem == null)
                return;
            versionSelected = ListBox.SelectedItem.ToString();
        }
        internal static void FileDownload(Socket s, string pathName, long size)
        {
            try
            {

                byte[] clientData = new byte[150000];

                int receivedBytesLen = s.Receive(clientData);

                using (BinaryWriter bWrite = new BinaryWriter(File.Open(pathName, FileMode.Append)))
                {
                    bWrite.Write(clientData, 0, receivedBytesLen);

                    int letti = receivedBytesLen;
                    size -= letti;

                    while (size > 0)
                    {
                        letti = s.Receive(clientData);
                        //Thread.Sleep(300);
                        bWrite.Write(clientData, 0, letti);
                        size -= letti;
                    }
                    Console.WriteLine("File: {0} ricevuto", pathName);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Download Fallito : " + ex.Message);
            }
        }
    }
}
