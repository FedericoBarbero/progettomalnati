﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Security.Cryptography;
using System.IO;
using System.Net;
using System.Net.Sockets;
using client;
using System.Threading;

namespace client
{
    /// <summary>
    /// Logica di interazione per Page1.xaml
    /// </summary>
    public partial class Page1 : Page
    {

        private string fileCred = @"C:\Users\Federico\Documents\progetto malnati\client\client\client\tmp\cred.txt";
        private string fileName = @"cred.txt";
        private static string tmpPath = @"C:\Users\Federico\Documents\progetto malnati\client\client\client\tmp";
        private static string credpath = @"C:\Users\Federico\Documents\progetto malnati\client\client\client\tmp\cred";
        private static string old_file;
        private static string new_file;

        public Page1()
        {
            InitializeComponent();
            try
            {
                Utilities.Empty(tmpPath);
            }
            catch(Exception ex)
            {
                Console.WriteLine("Error in ereasing temporary folder: " + ex.Message + ex.StackTrace);
            }
        }
        private void LogInButtonOnClick(object sender, RoutedEventArgs e)
        {
            if (!String.IsNullOrEmpty(UsernameLogIn.Text) && !String.IsNullOrEmpty(PasswordLogIn.Password))
            {
                var username = UsernameLogIn.Text;
                var password = generateMd5(PasswordLogIn.Password);
                //MessageBox.Show("username " + username + " Password " + password);

                if(Utilities.authenticate(fileCred, fileName, username, password, Utilities.LogMsg))
                {
                    MessageBox.Show("Logged In! Welcome " + username);
                    var appreference = App.Current as App;
                    appreference.user = username.ToString();                             
                    Console.WriteLine(appreference.user);
                    //Controllo e tento di ripristinare lo stato se esiste
                    restoreStatus(appreference.connectedSocket);
                    if (appreference.firstBackUp)
                    {
                        var MainFrame = appreference.MainFrame as Frame;
                        MainFrame.Navigate(new Uri("BackRestore.xaml", UriKind.Relative));
                    }
                    else
                    {
                        var MainFrame = appreference.MainFrame as Frame;
                        MainFrame.Navigate(new Uri("BackUp.xaml", UriKind.Relative));
                    }
                }
                else
                {
                    MessageBox.Show("Username and/or Password incorrect! Please try again.");
                }
                
                UsernameLogIn.Text = "";
                PasswordLogIn.Password = "";
            }
            else
            {
                MessageBox.Show("Username and/or password not valid. Please try again.");
                UsernameLogIn.Text = "";
                PasswordLogIn.Password = "";
            }
        }

        private static void restoreStatus(Socket s)
        {
            byte[] resp = new byte[3];
            s.Receive(resp);
            var appreference = App.Current as App;

            string rix = Encoding.ASCII.GetString(resp);
            if(rix.Equals("FRS"))
            {
                appreference.firstBackUp = true;
                return;
            }
            if(rix.Equals("XYZ"))
            {
                appreference.firstBackUp = false;
                appreference.monServiceOn = true;              
                if (File.Exists(credpath + "\\" + appreference.user + ".txt"))
                {
                    using (StreamReader sr = new StreamReader(new FileStream(credpath + "\\" + appreference.user + ".txt", FileMode.Open)))
                    {
                        appreference.RootFolderPath = sr.ReadLine();
                        appreference.RootFolder = sr.ReadLine();
                    }
                }
                else
                {
                    throw new Exception("file not found!");
                }
                riceviFile(appreference.connectedSocket);
                old_file = appreference.logFilePath;
                Utilities.generateLogFile();
                new_file = appreference.logFilePath;
                ThreadPool.QueueUserWorkItem(new WaitCallback(matchAndSync),null);           
                return;
            }
            throw new Exception("Error! wrong message!");
        }

        private static void matchAndSync(object o)
        {
            try
            {
                if (old_file.Equals(String.Empty) || new_file.Equals(String.Empty))
                    throw new Exception("Files not set");
                matchLogFiles(old_file, new_file);
                Sincronization.sincronize(null);
                Sincronization.startMonitoring();
                Sincronization.startSincronize();
                var appreference = App.Current as App;
                appreference.monServiceOn = true;
            }
            catch(Exception ex)
            {
                Console.WriteLine("error! Impossible to syncronize and match " + ex.Message + ex.StackTrace);
                throw new Exception("error! Impossible to syncronize and match");
            }
        }

        private static void matchLogFiles(string old_file, string new_file)
        {

            StreamReader sr1;
            StreamReader sr2;
            bool found = false;
            string line1, line2, path1,path2, md51, md52;
            try
            {
                using (sr1 = new StreamReader(new FileStream(old_file, FileMode.Open)))
                {
                    while ((line1 = sr1.ReadLine()) != null)
                    {
                        path1 = Utilities.getPathFromLogLine(line1);
                        md51 = Utilities.getMD5FromLogLine(line1);
                        using (sr2 = new StreamReader(new FileStream(new_file, FileMode.Open)))
                        {
                            while ((line2 = sr2.ReadLine()) != null && !found)
                            {
                                path2 = Utilities.getPathFromLogLine(line2);
                                md52 = Utilities.getMD5FromLogLine(line2);
                                if(path2.Equals(path1))
                                {
                                    found = true;
                                    if(!md51.Equals(md52))
                                    {
                                        Sincronization.writeUpdated(path1);
                                    }
                                }
                            }
                            if(!found)
                            {
                                Sincronization.writeDeleted(path1);
                            }
                            found = false;
                        }
                    }
                }

                using (sr2 = new StreamReader(new FileStream(new_file, FileMode.Open)))
                {
                    while ((line2 = sr2.ReadLine()) != null)
                    {
                        path2 = Utilities.getPathFromLogLine(line2);
                        md52 = Utilities.getMD5FromLogLine(line2);
                        using (sr1 = new StreamReader(new FileStream(old_file, FileMode.Open)))
                        {
                            while ((line1 = sr1.ReadLine()) != null && !found)
                            {
                                path1 = Utilities.getPathFromLogLine(line1);
                                md51 = Utilities.getMD5FromLogLine(line1);
                                if (path2.Equals(path1))
                                {
                                    found = true;
                                }
                            }
                            if (!found)
                            {
                                Sincronization.writeNew(path2);
                            }
                            found = false;
                        }
                    }
                }



            }
            catch (Exception ex)
            {
                Console.WriteLine("Error in Opening file log to be matched" + ex.Message + ex.StackTrace);
                throw new Exception("Error in Opening file log to be matched");
            }
        }

        //private static void matchLogFiles(string old_file, string new_file)
        //{

        //    StreamReader sr1;
        //    StreamReader sr2;
        //    try
        //    {
        //        sr1 = new StreamReader(new FileStream(old_file, FileMode.Open));
        //        sr2 = new StreamReader(new FileStream(new_file, FileMode.Open));
        //        int n = 0, m=0;
        //        string line1, line2, line3, line4;
        //        bool deleted = true;
        //        bool equal = false;

        //        while((line3 = sr1.ReadLine())!= null)
        //        {
        //            line1 = Utilities.getPathFromLogLine(line3);
        //            while ((line4 = sr2.ReadLine()) != null)
        //            {
        //                line2 = Utilities.getPathFromLogLine(line4);
        //                if (m < n)
        //                {
        //                    m++;
        //                    if (line1.Equals(line2))
        //                    {
        //                        equal = true;
        //                        break;
        //                    }
        //                    continue;
        //                }
        //                if (m == n)
        //                {
        //                    m++;
        //                    if (line1.Equals(line2))
        //                    {
        //                        equal = true;
        //                        break;
        //                    }
        //                    continue;
        //                }
        //                if(m > n)
        //                {
        //                    m++;
        //                    if (line1.Equals(line2))
        //                    {
        //                        deleted = false;
        //                        equal = false;
        //                        break;
        //                    }
        //                    continue;
        //                }
        //            }
        //            if (!equal)
        //            {
        //                if (!deleted)
        //                {
        //                    Sincronization.writeUpdated(line1);
        //                }
        //                else
        //                {
        //                    Sincronization.writeDeleted(line1);
        //                }
        //            }

        //            equal = false;
        //            n++;
        //            m = 0;
        //            deleted = true;
        //            sr2.Close();
        //            sr2 = new StreamReader(new FileStream(new_file, FileMode.Open));
        //        }
        //        n = 0;
        //        sr1.Close();
        //        sr2.Close();

        //        sr1 = new StreamReader(new FileStream(old_file, FileMode.Open));
        //        sr2 = new StreamReader(new FileStream(new_file, FileMode.Open));
        //        bool found = false;

        //        while ((line4 = sr2.ReadLine()) != null)
        //        {
        //            line2 = Utilities.getPathFromLogLine(line4);
        //            while ((line3 = sr1.ReadLine()) != null)
        //            {
        //                line1 = Utilities.getPathFromLogLine(line3);
        //                if(line2.Equals(line1))
        //                {
        //                    found = true;
        //                    break;
        //                }
        //            }
        //            if(!found)
        //            {
        //                Sincronization.writeNew(line2);
        //            }

        //            found = false;
        //            sr1.Close();
        //            sr1 = new StreamReader(new FileStream(old_file, FileMode.Open));
        //        }

        //        sr1.Close();
        //        sr2.Close();
        //    }
        //    catch(Exception ex)
        //    {
        //        Console.WriteLine("Error in Opening file log to be matched" + ex.Message + ex.StackTrace);
        //        throw new Exception("Error in Opening file log to be matched");
        //    }
        //}

        public static String riceviFile(Socket clientSock)
        {
            byte[] clientData = new byte[1024 * 5000];

            //Directory.CreateDirectory(receivedPath);
            int receivedBytesLen = clientSock.Receive(clientData);

            int fileNameLen = BitConverter.ToInt32(clientData, 0);
            string fileName = Encoding.ASCII.GetString(clientData, 4, fileNameLen);

            Console.WriteLine("Client:{0} connected & File {1} started received.", clientSock.RemoteEndPoint, fileName);

            IPEndPoint remote = clientSock.RemoteEndPoint as IPEndPoint;

            String s = tmpPath + "\\" + System.IO.Path.GetFileNameWithoutExtension(fileName) + ".txt";
            using (BinaryWriter bWrite = new BinaryWriter(File.Open(s, FileMode.Create)))
            {
                bWrite.Write(clientData, 4 + fileNameLen, receivedBytesLen - 4 - fileNameLen);
                var appreference = App.Current as App;
                appreference.logFilePath = s;
                Console.WriteLine("File: {0} received & saved at path: {1}", fileName, tmpPath);
            }
            return s;
        }
        private static string generateMd5(string src)
        {
            MD5 md5Hash = MD5.Create();
            string hash = GetMd5Hash(md5Hash, src);
            return hash;
        }

        private static string GetMd5Hash(MD5 md5Hash, string input)
        {

            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }

        // Verify a hash against a string.
        private static bool VerifyMd5Hash(MD5 md5Hash, string input, string hash)
        {
            // Hash the input.
            string hashOfInput = GetMd5Hash(md5Hash, input);

            // Create a StringComparer an compare the hashes.
            StringComparer comparer = StringComparer.OrdinalIgnoreCase;

            if (0 == comparer.Compare(hashOfInput, hash))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void ButtonSignUpOnClick(object sender, RoutedEventArgs e)
        {
            if (!String.IsNullOrEmpty(UsernameSignUp.Text) && !String.IsNullOrEmpty(PasswordSignUp.Password) && !String.IsNullOrEmpty(ConfirmPasswordSignUp.Password))
            {
                var username = UsernameSignUp.Text;
                var password = generateMd5(PasswordSignUp.Password);
                var password2 = generateMd5(ConfirmPasswordSignUp.Password);
                if (!password.Equals(password2))
                {
                    MessageBox.Show("Username and/or password not valid. Please try again.");
                    UsernameSignUp.Text = "";
                    PasswordSignUp.Password = "";
                    ConfirmPasswordSignUp.Password = "";
                    return;
                }

                //MessageBox.Show("username " + username + " Password " + password);

                if (Utilities.authenticate(fileCred, fileName, username, password, Utilities.RegMsg))
                    MessageBox.Show("Correctly Signed Up! Please Log in");
                else
                    MessageBox.Show("Username/Password invalid. Please try again.");

                UsernameSignUp.Text = "";
                PasswordSignUp.Password = "";
                ConfirmPasswordSignUp.Password = "";
                var appreference = App.Current as App;
                appreference.connectedSocket = null;
               
            }
            else
            {
                MessageBox.Show("Username and/or password not valid. Please try again.");
                UsernameSignUp.Text = "";
                PasswordSignUp.Password = "";
                ConfirmPasswordSignUp.Password = "";
                var appreference = App.Current as App;
                appreference.connectedSocket = null;
            }
        }

        private void GoOnbutton_Click(object sender, RoutedEventArgs e)
        {
            //MessageBox.Show("Ok");
            var appreference = App.Current as App;
            var MainFrame = appreference.MainFrame as Frame;
            MainFrame.Navigate(new Uri("BackRestore.xaml", UriKind.Relative));
        }
    }
}
