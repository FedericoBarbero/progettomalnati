﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using client;
using System.IO;
using System.Threading;
using System.Collections;
using System.Windows;

namespace client
{
    class Sincronization
    {
        private static string path2 = @"C:\Users\Federico\Documents\progetto malnati\client\client\client\tmp";
        private static string modified = @"C:\Users\Federico\Documents\progetto malnati\client\client\client\tmp\modified.txt";
        private static string deleted = @"C:\Users\Federico\Documents\progetto malnati\client\client\client\tmp\deleted.txt";
        private static string created = @"C:\Users\Federico\Documents\progetto malnati\client\client\client\tmp\created.txt";
        private static string addMsg = "ADD";
        private static string updMsg = "UPD";
        private static string delMsg = "DEL";
        private static string logMsg = "LOG";
        private static Mutex m = new Mutex();
        private static List<FileSystemWatcher> listOfWatchers = new List<FileSystemWatcher>();
        //private static FileSystemWatcher fsw = new FileSystemWatcher();
        private static Timer t;

        internal static void startSincronize()
        {
            TimerCallback tc = new TimerCallback(sincronize);
            t = new Timer(tc, null, 60000, 60000); 
            Console.WriteLine("Starting sincronization process");
            var appreference = App.Current as App;
            appreference.monitoring = true;
        }

        internal static void stopSincronize()
        {
            t.Dispose();
            t = null;
            if (File.Exists(created) && File.Exists(modified) && File.Exists(deleted))
                deleteMonFiles();
            else
            {
                Console.WriteLine("Error! File monitoring already deleted!");
                //throw new Exception("Error! File monitoring already deleted!");
            }
            Console.WriteLine("Stopped sincronization process");
            var appreference = App.Current as App;
            appreference.monitoring = false;
        }

        internal static void sincronize(Object o)
        {
            var appreference = App.Current as App;
            Socket s = appreference.connectedSocket;

            appreference.msincro.WaitOne();
                appreference.sincronizing = true;
            appreference.msincro.ReleaseMutex();
            
            m.WaitOne();
            try
            {
                //SEND DELETED FILE

                //Read deleted file and tell the server
                StreamReader sr;
                string line = null;     

                if (File.Exists(deleted))
                {
                    //logtosend = true;
                    sr = new StreamReader(new FileStream(deleted, FileMode.Open));                    
                    while ((line = sr.ReadLine()) != null)
                    {
                        //build path to be sent to the server
                        char[] delimiterChars = { '\\' };
                        string[] words = line.Split(delimiterChars);

                        string path = @"\root";
                        int flag = 0;

                        foreach (string str in words)
                        {
                            if (flag != 0)
                                path += "\\" + str;
                            if (str.CompareTo(appreference.RootFolder) == 0)
                                flag = 1;
                        }

                        //send path to server
                        byte[] msg = Encoding.ASCII.GetBytes(delMsg);
                        s.Send(msg);
                        byte[] buffer = Encoding.ASCII.GetBytes(path);
                        s.Send(buffer);
                        var ack_o_err = AttendiAck(s);
                        if (ack_o_err == "ERR")
                        {
                            Console.WriteLine("Errore nel trasferimento di del file :");
                            throw new Exception("Errore nel trasferimento dei file deleted");
                        }
                    }
                    sr.Close();
                    //SEND NEW LOG FILE
                    //Send msg
                    byte[] msg2 = Encoding.ASCII.GetBytes(logMsg);
                    s.Send(msg2);
                    Utilities.sendLogFile(s);
                    var ack_o_err2 = AttendiAck(s);
                    if (ack_o_err2 == "ERR")
                    {
                        Console.WriteLine("Errore nel trasferimento di log file");
                        throw new Exception("Errore nel trasferimento file log");
                    }
                }
                //SEND MODIFIED FILES
                if (File.Exists(modified))
                {
                    //Read modified file and tell the server
                    sr = new StreamReader(new FileStream(modified, FileMode.Open));
                    line = null;
                    while ((line = sr.ReadLine()) != null)
                    {
                        //build path to be sent to the server
                        char[] delimiterChars = { '\\' };
                        string[] words = line.Split(delimiterChars);

                        string path = @"\root";
                        int flag = 0;

                        foreach (string str in words)
                        {
                            if (flag != 0)
                                path += "\\" + str;
                            if (str.CompareTo(appreference.RootFolder) == 0)
                                flag = 1;
                        }


                        //Send msg
                        byte[] msg = Encoding.ASCII.GetBytes(updMsg);
                        s.Send(msg);

                        //send file + metadata                   
                        string ack_o_err;

                        string nome = NomeFileDaPath(line);
                        //var fileFullName = appreference.RootFolderPath + subPathFile(line);
                        if (nome != null)
                            FileUpload(s, path ,line);
                        ack_o_err = AttendiAck(s);
                        if (ack_o_err == "ERR")
                        {
                            Console.WriteLine("Errore nel trasferimento di {0} in upd file mandati:", nome);
                            throw new Exception("Errore nel trasferimento file upd");
                        }
                        if (nome != null)
                            //Utilities.sendMetaSerialized(s, line);
                            Utilities.sendMetaFile(s, line);
                        ack_o_err = AttendiAck(s);
                        if (ack_o_err == "ERR")
                        {
                            Console.WriteLine("Errore nel trasferimento di {0} in upd file mandati:", nome);
                            throw new Exception("Errore nel trasferimento file upd");
                        }
                    }
                    Console.WriteLine("All files correctly sent!");
                    sr.Close();
                    //SEND NEW LOG FILE
                    //Send msg
                    byte[] msg2 = Encoding.ASCII.GetBytes(logMsg);
                    s.Send(msg2);
                    Utilities.sendLogFile(s);
                    var ack_o_err2 = AttendiAck(s);
                    if (ack_o_err2 == "ERR")
                    {
                        Console.WriteLine("Errore nel trasferimento di log file");
                        throw new Exception("Errore nel trasferimento file log");
                    }
                }
                if (File.Exists(created))
                {
                    //SEND NEW FILES
                    sr = new StreamReader(new FileStream(created, FileMode.Open));
                    line = null;
                    while ((line = sr.ReadLine()) != null)
                    {
                        //build path to be sent to the server
                        char[] delimiterChars = { '\\' };
                        string[] words = line.Split(delimiterChars);

                        string path = @"\root";
                        int flag = 0;

                        foreach (string str in words)
                        {
                            if (flag != 0)
                                path += "\\" + str;
                            if (str.CompareTo(appreference.RootFolder) == 0)
                                flag = 1;
                        }

                        //Send msg
                        byte[] msg = Encoding.ASCII.GetBytes(addMsg);
                        s.Send(msg);

                        //send file + metadata                   
                        string ack_o_err;

                        string nome = NomeFileDaPath(line);
                        //var fileFullName = appreference.RootFolderPath + subPathFile(line);
                        if (nome != null)
                            FileUpload(s, nome, line);
                        ack_o_err = AttendiAck(s);
                        if (ack_o_err == "ERR")
                        {
                            Console.WriteLine("Errore nel trasferimento di {0} in add file mandati:", nome);
                            throw new Exception("Errore nel trasferimento file add");
                        }
                        if (nome != null)
                            //Utilities.sendMetaSerialized(s, line);
                            Utilities.sendMetaFile(s, line);
                        ack_o_err = AttendiAck(s);
                        if (ack_o_err == "ERR")
                        {
                            Console.WriteLine("Errore nel trasferimento di {0} in add file mandati:", nome);
                            throw new Exception("Errore nel trasferimento file add");
                        }
                    }
                    Console.WriteLine("All files correctly sent!");
                    sr.Close();
                    //SEND NEW LOG FILE
                    //Send msg
                    byte[] msg2 = Encoding.ASCII.GetBytes(logMsg);
                    s.Send(msg2);
                    Utilities.sendLogFile(s);
                    var ack_o_err2 = AttendiAck(s);
                    if (ack_o_err2 == "ERR")
                    {
                        Console.WriteLine("Errore nel trasferimento di log file");
                        throw new Exception("Errore nel trasferimento file log");
                    }
                }

                //if (File.Exists(modified) || File.Exists(created) || File.Exists(deleted))
                //{
                //SEND NEW LOG FILE
                //Send msg
                byte[] msg3 = Encoding.ASCII.GetBytes(logMsg);
                s.Send(msg3);
                Utilities.sendLogFile(s);
                var ack_o_err3 = AttendiAck(s);
                if (ack_o_err3 == "ERR")
                {
                    Console.WriteLine("Errore nel trasferimento di log file");
                    throw new Exception("Errore nel trasferimento file log");
                }

                //}

            }
            catch (Exception e)
            {
                Console.WriteLine("Error in sincronize func.. : " + e.Message + e.StackTrace);
            }

            //delete support files
            try {
                if (File.Exists(modified))
                    File.Delete(modified);
                if (File.Exists(deleted))
                    File.Delete(deleted);
                if (File.Exists(created))
                    File.Delete(created);
            }
            catch(Exception ex)
            {
                Console.WriteLine("Error in deleting support file after a restore. " + ex.Message + ex.StackTrace);
            }
            m.ReleaseMutex();

            appreference.msincro.WaitOne();
                appreference.sincronizing = false;
            appreference.msincro.ReleaseMutex();            
        }

        internal static void startMonitoring()
        {
            var appreference = App.Current as App;
            startMonitoring(0, appreference.RootFolderPath);
        }
        private static void startMonitoring(int level, string path)
        {
            if (level == 0)
            {
                FileSystemWatcher fsw = new FileSystemWatcher();
                fsw.Path = path;
                fsw.Filter = "";
                fsw.NotifyFilter = NotifyFilters.CreationTime | NotifyFilters.FileName | NotifyFilters.DirectoryName | NotifyFilters.LastAccess | NotifyFilters.LastWrite;

                fsw.Renamed += Fsw_Renamed;
                fsw.Changed += Fsw_Changed;
                fsw.Created += Fsw_Created;
                fsw.Deleted += Fsw_Deleted;
                fsw.Error += Fsw_Error;
                fsw.Disposed += Fsw_Disposed;

                fsw.EnableRaisingEvents = true;

                listOfWatchers.Add(fsw);
            }

            foreach (string d in Directory.GetDirectories(path))
            {
                FileSystemWatcher fsw1 = new FileSystemWatcher();
                fsw1.Path = d;
                fsw1.Filter = "";
                fsw1.NotifyFilter = NotifyFilters.CreationTime | NotifyFilters.FileName | NotifyFilters.DirectoryName | NotifyFilters.LastAccess | NotifyFilters.LastWrite;

                fsw1.Renamed += Fsw_Renamed;
                fsw1.Changed += Fsw_Changed;
                fsw1.Created += Fsw_Created;
                fsw1.Deleted += Fsw_Deleted;
                fsw1.Error += Fsw_Error;
                fsw1.Disposed += Fsw_Disposed;

                fsw1.EnableRaisingEvents = true;

                listOfWatchers.Add(fsw1);

                startMonitoring(++level, d);
            }
        }

        private static void Fsw_Renamed(object sender, RenamedEventArgs e)
        {
            m.WaitOne();
            var old_name = e.OldFullPath;
            var new_name = e.FullPath;
            if (Path.HasExtension(new_name))
            {
                try
                {
                    string line;
                    if (File.Exists(created))
                    {
                        using (StreamReader sr = new StreamReader(new FileStream(created, FileMode.Open)))
                        {
                            using (StreamWriter sw = new StreamWriter(new FileStream(path2 + "\\tmp_created.txt", FileMode.Create)))
                            {
                                while ((line = sr.ReadLine()) != null)
                                {
                                    if (old_name.Equals(line))
                                    {
                                        sw.WriteLine(new_name);
                                    }
                                    else
                                    {
                                        sw.WriteLine(line);
                                    }
                                }
                            }
                        }
                        File.Delete(created);
                        File.Move(path2 + "\\tmp_created.txt", created);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Exception in renaming created file " + ex.Message);
                }
                try
                {
                    string line;
                    if (File.Exists(modified))
                    {
                        using (StreamReader sr = new StreamReader(new FileStream(modified, FileMode.Open)))
                        {
                            using (StreamWriter sw = new StreamWriter(new FileStream(path2 + "\\tmp_modified.txt", FileMode.Create)))
                            {
                                while ((line = sr.ReadLine()) != null)
                                {
                                    if (old_name.Equals(line))
                                    {
                                        sw.WriteLine(new_name);
                                    }
                                    else
                                    {
                                        sw.WriteLine(line);
                                    }
                                }
                            }
                        }
                        File.Delete(modified);
                        File.Move(path2 + "\\tmp_modified.txt", modified);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Exception in renaming modified file " + ex.Message);
                } 
            }
            ////SEND NEW LOG FILE
            ////Send msg            
            //var appreference = App.Current as App;
            //Socket s = appreference.connectedSocket;
            //byte[] msg = Encoding.ASCII.GetBytes(logMsg);
            //s.Send(msg);
            //Utilities.sendLogFile(s);
            //var ack_o_err = AttendiAck(s);
            //if (ack_o_err == "ERR")
            //{
            //    Console.WriteLine("Errore nel trasferimento di log file");
            //    throw new Exception("Errore nel trasferimento file log");
            //}
            m.ReleaseMutex();
            ThreadPool.QueueUserWorkItem(new WaitCallback(sincronize), null);
        }

        internal static void stopMonitoring()
        {
            foreach (FileSystemWatcher fsw in listOfWatchers)
            { 
                fsw.EnableRaisingEvents = false;
                fsw.Dispose();
            }
            listOfWatchers.Clear();            
        }

        private static void Fsw_Disposed(object sender, EventArgs e)
        {
            Console.WriteLine("(Error) in fileSystemWatcher.. (Disposed)");
        }

        private static void Fsw_Error(object sender, ErrorEventArgs e)
        {
            Console.WriteLine("Error in fileSystemWatcher..");
        }

        private static void Fsw_Deleted(object sender, FileSystemEventArgs e)
        {
            m.WaitOne();

            if(Path.HasExtension(e.FullPath))
            {
                try
                {
                    using (StreamWriter sw = new StreamWriter(new FileStream(deleted, FileMode.Append)))
                    {
                        //sw.WriteLine("Path:" + e.FullPath + ":Name:" + e.Name);                       
                        sw.WriteLine(e.FullPath);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Exception in writing deleted file " + ex.Message + ex.StackTrace);
                }
            }
            else
            {
                var appreference = App.Current as App;
                var old_log = appreference.logFilePath;
                //Utilities.generateLogFile("tmp.txt");
                //var new_log = path2 + "\\tmp.txt";
                using (StreamReader sr = new StreamReader(new FileStream(old_log, FileMode.Open)))
                {
                    string line;
                    bool equal = true;
                    char[] del = { '\\' };
                    string[] words1 = Utilities.buildRootPath(e.FullPath).Split(del);
                    while ((line = sr.ReadLine())!= null)
                    {
                        string[] words2 = line.Split(del);
                        
                        for(int i=0;i<words1.Length; i++)
                        {
                            if(!words1[i].Equals(words2[i]))
                            {
                                equal = false;
                                break;
                            }                            
                        }
                        if(equal)
                        {
                            try
                            {
                                using (StreamWriter sw = new StreamWriter(new FileStream(deleted, FileMode.Append)))
                                {                      
                                    sw.WriteLine(Utilities.revertRootPath(Utilities.getPathFromLogLine(line)));

                                }
                                foreach(FileSystemWatcher fsw in listOfWatchers)
                                {
                                    if(fsw.Path.Equals(Utilities.revertRootPath(Utilities.getPathFromLogLine(line))))
                                    {
                                        fsw.EnableRaisingEvents = false;
                                        fsw.Dispose();
                                        listOfWatchers.RemoveAt(listOfWatchers.IndexOf(fsw));
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine("Exception in writing deleted file " + ex.Message + ex.StackTrace);
                            }
                        }
                        equal = true;
                    }
                }
            }
           

                m.ReleaseMutex();
        }

        private static void Fsw_Created(object sender, FileSystemEventArgs e)
        {
            m.WaitOne();

            //MessageBox.Show("Created event");
            if(Path.HasExtension(e.FullPath))
            {

                try
                {
                    using (StreamWriter sw = new StreamWriter(new FileStream(created, FileMode.Append)))
                    {
                        sw.WriteLine(e.FullPath);
                        //sw.WriteLine("Path:" + e.FullPath + ":Name:" + e.Name);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Exception in writing created file " + ex.Message + ex.StackTrace);
                }

            }
            else
            {
                FileSystemWatcher fsw = new FileSystemWatcher();
                fsw.Path = e.FullPath;
                fsw.Filter = "";
                fsw.NotifyFilter = NotifyFilters.CreationTime | NotifyFilters.FileName | NotifyFilters.LastAccess | NotifyFilters.LastWrite;

                fsw.Renamed += Fsw_Renamed;
                fsw.Changed += Fsw_Changed;
                fsw.Created += Fsw_Created;
                fsw.Deleted += Fsw_Deleted;
                fsw.Error += Fsw_Error;
                fsw.Disposed += Fsw_Disposed;

                fsw.EnableRaisingEvents = true;

                listOfWatchers.Add(fsw);
            }
            m.ReleaseMutex();
        }

        private static void Fsw_Changed(object sender, FileSystemEventArgs e)

        {
            m.WaitOne();
            //MessageBox.Show("Changed event");
            if(Path.HasExtension(e.FullPath))
            {
                try
                {
                    string line = "";
                    StreamReader sr = null;
                    if (File.Exists(created))
                    {
                        sr = new StreamReader(new FileStream(created, FileMode.Open));

                        while ((line = sr.ReadLine()) != null)
                        {
                            if (line.Equals(e.FullPath))
                            {
                                sr.Close();
                                m.ReleaseMutex();
                                return;
                            }
                        }
                        sr.Close();
                    }
                    if (File.Exists(modified))
                    {
                        sr = new StreamReader(new FileStream(modified, FileMode.Open));
                        line = "";
                        while ((line = sr.ReadLine()) != null)
                        {
                            if (line.Equals(e.FullPath))
                            {
                                sr.Close();
                                m.ReleaseMutex();
                                return;
                            }
                        }
                        sr.Close();
                    }
                    using (StreamWriter sw = new StreamWriter(new FileStream(modified, FileMode.Append)))
                    {
                        sw.WriteLine(e.FullPath);
                        //sw.WriteLine("Path:" + e.FullPath + ":Name:" + e.Name);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Exception in writing modified file " + ex.Message + ex.StackTrace);
                } 
            }

            m.ReleaseMutex();
        }

        public static void deleteMonFiles()
        {
            try
            {
                m.WaitOne();

                File.Delete(modified);
                File.Delete(created);
                File.Delete(deleted);

                m.ReleaseMutex();
            }
            catch (Exception e)
            {
                Console.WriteLine("Error in deleting mon file :" + e.Message + e.StackTrace);
            }
        }

        private static string AttendiAck(Socket s)
        {
            byte[] buff = new byte[100];
            int ack = -1;
            try
            {
                ack = s.Receive(buff);
            }
            catch (SocketException se)
            {
                Console.WriteLine("Socket exception! : {0}", se.Message);
            }
            int fileNameLen;
            if (ack != 0)
            {
                fileNameLen = BitConverter.ToInt32(buff, 0);
                if (fileNameLen == 0)
                    return "OK";
            }
            return "ERR";
        }
        public static void FileUpload(Socket s, string filename, string path)
        {
            byte[] fileNameByte = Encoding.ASCII.GetBytes(filename);

            byte[] fileData = File.ReadAllBytes(path);

            FileInfo f = new FileInfo(path); // per trovare la lunghezza in byte del file

            /* 4 byte => 1 int per la lunghezza del nome del file +
             * il numero di byte della lunghezza del nome file +
             * numero di byte legato alla dimensione dei dati */
            byte[] fileLen = BitConverter.GetBytes(f.Length);
            byte[] clientData = new byte[4 + fileNameByte.Length + 8 + fileData.Length];
            byte[] fileNameLen = BitConverter.GetBytes(fileNameByte.Length);

            Console.WriteLine("fileName.length: " + fileNameByte.Length + " bytes: " + fileNameLen[0]);
            Console.WriteLine("fileLength: " + f.Length +"  bytes: "+fileLen[0]);

            fileNameLen.CopyTo(clientData, 0);
            fileNameByte.CopyTo(clientData, 4);
            fileLen.CopyTo(clientData, 4 + fileNameByte.Length);
            fileData.CopyTo(clientData, 4 + fileNameByte.Length + 8);


            ArrayList DataClient = new ArrayList();

            DataClient = Split(clientData);
            try
            {
                foreach (object o in DataClient)
                {

                    s.Send((byte[])o);
                }
            }
            catch (SocketException se)
            {
                Console.WriteLine("Socket Exception!! {0}", se.Message);
            }

            Console.WriteLine("File:{0} è stato inviato ", filename);

        }
        //public static void FileUpload(Socket s, string path)
        //{
        //    byte[] fileNameByte = Encoding.ASCII.GetBytes(path);

        //    byte[] fileData = File.ReadAllBytes(path);

        //    FileInfo f = new FileInfo(path); // per trovare la lunghezza in byte del file

        //    /* 4 byte => 1 int per la lunghezza del nome del file +
        //     * il numero di byte della lunghezza del nome file +
        //     * numero di byte legato alla dimensione dei dati */
        //    byte[] fileLen = BitConverter.GetBytes(f.Length);
        //    byte[] clientData = new byte[4 + fileNameByte.Length + 8 + fileData.Length];
        //    byte[] fileNameLen = BitConverter.GetBytes(fileNameByte.Length);

        //    fileNameLen.CopyTo(clientData, 0);
        //    fileNameByte.CopyTo(clientData, 4);
        //    fileLen.CopyTo(clientData, 4 + fileNameByte.Length);
        //    fileData.CopyTo(clientData, 4 + fileNameByte.Length + 8);


        //    ArrayList DataClient = new ArrayList();

        //    DataClient = Split(clientData);
        //    try
        //    {
        //        foreach (object o in DataClient)
        //        {

        //            s.Send((byte[])o);
        //        }
        //    }
        //    catch (SocketException se)
        //    {
        //        Console.WriteLine("Socket Exception!! {0}", se.Message);
        //    }

        //    Console.WriteLine("File:{0} è stato inviato ", path);

        //}

        private static ArrayList Split(byte[] filebytes)
        {
            int range = 1024 * 1024;
            int pos = 0;
            int remaining;

            ArrayList result = new ArrayList();

            while ((remaining = filebytes.Length - pos) > 0)
            {
                byte[] block = new byte[Math.Min(remaining, range)];

                Array.Copy(filebytes, pos, block, 0, block.Length);
                result.Add(block);

                pos += block.Length;
            }

            return result;
        }

        private static string subPathFile(string path)
        {
            char[] delimiterChars = { '\\' };
            string[] words = path.Split(delimiterChars);
            string result = "";

            for (int i = 2; i < words.Length; i++)
            {
                result += "\\" + words[i];
            }
            return result;
        }

        private static string NomeFileDaPath(string path)
        {
            char[] delimiterChars = { '\\' };
            string[] words = path.Split(delimiterChars);

            foreach (string s in words)
            {

                if (s.IndexOf(".") != -1 && s.CompareTo("") != 0)
                {
                    return s;
                }

            }
            return null;
        }
        internal static void writeDeleted(string file)
        {
            var path = Utilities.revertRootPath(file);
            m.WaitOne();
            try
            {
                using (StreamWriter sw = new StreamWriter(new FileStream(deleted, FileMode.Append)))
                {
                    sw.WriteLine(path);
                    //sw.WriteLine("Path:" + e.FullPath + ":Name:" + e.Name);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception in writing created file " + ex.Message + ex.StackTrace);
            }
            m.ReleaseMutex();
        }
        internal static void writeUpdated(string file)
        {
            var path = Utilities.revertRootPath(file);
            m.WaitOne();
            try
            {
                using (StreamWriter sw = new StreamWriter(new FileStream(modified, FileMode.Append)))
                {
                    sw.WriteLine(path);
                    //sw.WriteLine("Path:" + e.FullPath + ":Name:" + e.Name);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception in writing created file " + ex.Message + ex.StackTrace);
            }
            m.ReleaseMutex();
        }
        internal static void writeNew(string file)
        {
            var path = Utilities.revertRootPath(file);
            m.WaitOne();
            try
            {
                using (StreamWriter sw = new StreamWriter(new FileStream(created, FileMode.Append)))
                {
                    sw.WriteLine(path);
                    //sw.WriteLine("Path:" + e.FullPath + ":Name:" + e.Name);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception in writing created file " + ex.Message + ex.StackTrace);
            }
            m.ReleaseMutex();
        }
    }
}
